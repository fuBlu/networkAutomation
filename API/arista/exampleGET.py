import requests
from requests.auth import HTTPBasicAuth
import json

auth = HTTPBasicAuth('ntc', 'ntc123')
headers  = { 'Content-Type': 'application/json' }

version = {
  "jsonrpc": "2.0",
  "method": "runCmds",
  "params": {
    "format": "json",
    "timestamps": False,
    "autoComplete": False,
    "expandAliases": False,
    "cmds": [
      "show version"
    ],
    "version": 1
  },
  "id": "EapiExplorer-1"
}

url = 'http://130.211.68.23/command-api'

response = requests.post(url, data=json.dumps(version), headers=headers, auth=auth)

print(response.text)
type(response.text)

data = json.loads(response.text)
print(data)
type(data)

print(json.dumps(data, indent=4))

print('This is the MAC from show version: {}'.format(data['result'][0]['systemMacAddress']))

result = data['result'][0]
print('This is the same MAC using .get methond: {}'.format(result.get('systemMacAddress')))
