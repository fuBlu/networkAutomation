#!/usr/bin/env python3
import re
from credPass import credPass
from netmiko import ConnectHandler

acl = '''access-list outside extended permit tcp object-group rdp_clients object-group clisys_lca2_nets object-group rdp log notifications'''

def credential():
    fw = credPass()
    username = fw.load('vfw1-venuemaster-lon1.netops.tmcs','username')
    password = fw.load('vfw1-venuemaster-lon1.netops.tmcs','password')
    enable = fw.load('vfw1-venuemaster-lon1.netops.tmcs','enable')
    return username, password, enable

def main(username,password,enable,acl):
    finalConf = []
    objs = []
    sshSession = ConnectHandler(device_type='cisco_asa', ip='vfw1-venuemaster-lon1.netops.tmcs', username=username, password=password, secret=enable)
    objectGroup = re.findall(r'(?<=object-group\s)\w+', acl)
    for objId in objectGroup:
        objects = sshSession.send_command('show running-config object-group id {}'.format(objId))
        objs.extend(x for x in objects.split('\n') if x)
    finalConf = ['no {}'.format(k.replace('group-object', 'object-group')) for k in objs if 'port-object' not in k and 'network-object' not in k]
    return finalConf

if __name__ == '__main__':
    main(*credential(),acl)
