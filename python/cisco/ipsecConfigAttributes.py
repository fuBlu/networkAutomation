#!/usr/bin/env python3
import sys, time, re
from sshASA import sshHost

def main(host):
  partner = input('Give me the name of the 3PPartner > ')
  output_0 = host.send_command('show running-config crypto map | i ' + str(partner) + '\n')
  time.sleep(1)
  print(output_0)
  crypto = input('Which crypto map you want see? > ')
  for i in crypto:
    output_1 = host.send_command('show running-config crypto map | i _' + str(crypto) + '_\n')
    time.sleep(.5)
  print(output_1)
  crypto_acl = re.findall(r'(?<=address\s)[-\w+]*', output_1)
  for i in crypto_acl:
    output_3 = host.send_command('show running-config access-list ' + i + '\n')
    time.sleep(.5)
  print(output_3)
  acl_entry = re.findall(r'(?<=object-group\s)[-\w+]*', output_3)
  for i in acl_entry:
    shRun1 = host.send_command('show running-config object-group id ' + i)
    time.sleep(.5)
    shRun2 = host.send_command('show running-config access-list transit-in | i ' + i + '\n')
    time.sleep(.5)
    shRun3 = host.send_command('show running-config access-list outside-in | i ' + i + '\n')
    time.sleep(.5)
    shRun4 = host.send_command('show running-config nat | i ' + i + '\n')
    time.sleep(.5)
    print(shRun1 + '\n' + shRun2 + '\n' + shRun3 +'\n' + shRun4)

if __name__ == '__main__':
    main(sshHost(sys.argv[1]))

#f20180423
