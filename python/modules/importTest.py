#!/usr/bin/env python3
import sys
from sshJUNOS import sshHost as ssh

def showVersion(host):
    out = host.send_command("show version")
    print(out)

showVersion(ssh(sys.argv[1]))
