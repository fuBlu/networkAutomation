#!/usr/bin/env python3

import yaml, sys, ipaddress, socket, getpass, argparse
from netmiko import ConnectHandler
from netaddr import IPAddress

objGrSList = []
objGrDList = []
finalConf = []
dstAddress = []
appDicTCP = { 80:'junos-http', 443:'junos-https', 22:'junos-ssh', 21:'junos-ftp', 53:'junos-dns-tcp', 'any':'junos-tcp-any'}
appDicUDP = { 123:'junos-ntp', 25:'junos-snmp', 53:'junos-dns-udp', 514:'junos-syslog', 'any':'junos-udp-any' }

def queryYesNo(question, default="yes"):
    valid = {"yes": True, "y": True, "ye": True, "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: ".format(default))

    while True:
        print(question + prompt)
        choice = input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            print("Please respond with 'yes' or 'no' ""(or 'y' or 'n').\n")

def main():
    # flags
    parser = argparse.ArgumentParser(description="Juniper SRX policy configurator - Version 2.7 180626")
    parser.add_argument('--data-input', '-d', dest='data_input', help="YAML file data with source, destination IP, ports and protocol",type= str)
    parser.add_argument('--firewall-list','-f', dest='firewall_list', help="YAML file with firewalls list and corresping connected/static subnets", type= str)
    parser.add_argument('--check', '-C', dest='check', action='store_true',help="Generate the config only - don't write config to the firewall")
    parser.add_argument('--execute', '-X', dest='execute', action='store_true', help="Generate the config and copy it to the firewall")

    args = parser.parse_args()
    inventory = args.data_input
    firewalls = args.firewall_list
    check = args.check
    execute = args.execute

    if not firewalls or not inventory:
        print ('Please indicate the yaml files with the list of firewalls and the data-input file. i.e. "python3 aclConfigurator.py -d CHG0107388.yaml -f firewallList.yaml -C"')
        sys.exit(1)

    inventory = yaml.load(open(inventory, 'rb'))
    firewalls = yaml.load(open(firewalls, 'rb'))

    if not check or execute:
        print ("Please indicate -C or -X")

    for key in inventory:
        if 'zones' in key:
            srcZone = key['zones']['source']
            dstZone = key['zones']['destination']
    # Create destination address-book
    if check:
        for destinations in inventory[2]['Destinations']:
            if destinations['ipDestination']['mask'] == '255.255.255.255':
                for ip in destinations['ipDestination']['address']:
                    cidr = IPAddress(destinations['ipDestination']['mask']).netmask_bits()
                    finalConf.append('set security address-book {} address H-{}-{} {}/{}'.format(dstZone,ip,cidr,ip,cidr))
                    objGrDList.append('H-{}-{}'.format(ip,cidr))
                    dstAddress.append(ip)
            else:
                for ip in destinations['ipDestination']['address']:
                    cidr = IPAddress(destinations['ipDestination']['mask']).netmask_bits()
                    finalConf.append('set security address-book {} address N-{}-{} {}/{}'.format(dstZone,ip,cidr,ip,cidr))
                    objGrDList.append('N-{}-{}'.format(ip,cidr))
                    dstAddress.append(ip)

            # Create source address-book
            for sources in inventory[1]['Sources']:
                for ip in sources['ipSource']['address']:
                    if sources['ipSource']['mask'] == '255.255.255.255':
                        cidr = IPAddress(sources['ipSource']['mask']).netmask_bits()
                        finalConf.append('set security address-book {} address H-{}-{} {}/{}'.format(srcZone,ip,cidr,ip,cidr))
                        objGrSList.append('H-{}-{}'.format(ip,cidr))
                    else:
                        cidr = IPAddress(sources['ipSource']['mask']).netmask_bits()
                        finalConf.append('set security address-book {} address N-{}-{} {}/{}'.format(srcZone,ip,cidr,ip,cidr))
                        objGrSList.append('N-{}-{}'.format(ip,cidr))

            # Create destination address-set
            for i in objGrDList:
                finalConf.append('set security address-book {} address-set DG-{} address {}'.format(dstZone,inventory[0]['SNOW']['change'],i))
            DGLite = ('DG-{}'.format(inventory[0]['SNOW']['change']))
            objGrDList.clear()

            # Create source address-set
            for i in objGrSList:
                finalConf.append('set security address-book {} address-set SG-{} address {}'.format(srcZone,inventory[0]['SNOW']['change'],i))
            SGLite = ('SG-{}'.format(inventory[0]['SNOW']['change']))
            objGrSList.clear()

            # Create policies
            finalConf.append('set security policies from-zone {} to-zone {} policy {} match source-address {}'.format(srcZone,dstZone,inventory[0]['SNOW']['change'],SGLite))
            finalConf.append('set security policies from-zone {} to-zone {} policy {} match destination-address {}'.format(srcZone,dstZone,inventory[0]['SNOW']['change'],DGLite))


            # find existin application or create new
            for key in inventory:
                if 'protPortDestination' in key:
                    if key['protPortDestination']['protocol'] == 'tcp':
                        for port in key['protPortDestination']['ports']:
                            if port in appDicTCP.keys():
                                finalConf.append('set security policies from-zone {} to-zone {} policy {} match application {}'.format(srcZone,dstZone,inventory[0]['SNOW']['change'],appDicTCP[port]))
                            elif port not in appDicTCP.keys():
                                finalConf.append('set applications application TCP-{} protocol tcp destination-port {}'.format(port,port))
                                finalConf.append('set security policies from-zone {} to-zone {} policy {} match application TCP-{}'.format(srcZone,dstZone,inventory[0]['SNOW']['change'],port))

                    elif key['protPortDestination']['protocol'] == 'udp':
                        for port in key['protPortDestination']['ports']:
                            if port in appDicUDP.keys():
                                finalConf.append('set security policies from-zone {} to-zone {} policy {} match application {}'.format(srcZone,dstZone,inventory[0]['SNOW']['change'],appDicUDP[port]))
                            elif port not in appDicUDP.keys():
                                finalConf.append('set applications application UDP-{} protocol udp destination-port {}'.format(port,port))
                                finalConf.append('set security policies from-zone {} to-zone {} policy {} match application UDP-{}'.format(srcZone,dstZone,inventory[0]['SNOW']['change'],port))

            finalConf.append('set security policies from-zone {} to-zone {} policy {} then permit'.format(srcZone,dstZone,inventory[0]['SNOW']['change']))

        for config in finalConf:
            print(config)

    if execute:
        for destinations in inventory[2]['Destinations']:
            if destinations['ipDestination']['mask'] == '255.255.255.255':
                for ip in destinations['ipDestination']['address']:
                    cidr = IPAddress(destinations['ipDestination']['mask']).netmask_bits()
                    finalConf.append('set security address-book {} address H-{}-{} {}/{}'.format(dstZone,ip,cidr,ip,cidr))
                    objGrDList.append('H-{}-{}'.format(ip,cidr))
                    dstAddress.append(ip)
            else:
                for ip in destinations['ipDestination']['address']:
                    cidr = IPAddress(destinations['ipDestination']['mask']).netmask_bits()
                    finalConf.append('set security address-book {} address N-{}-{} {}/{}'.format(dstZone,ip,cidr,ip,cidr))
                    objGrDList.append('N-{}-{}'.format(ip,cidr))
                    dstAddress.append(ip)

            # Create source address-book
            for sources in inventory[1]['Sources']:
                for ip in sources['ipSource']['address']:
                    if sources['ipSource']['mask'] == '255.255.255.255':
                        cidr = IPAddress(sources['ipSource']['mask']).netmask_bits()
                        finalConf.append('set security address-book {} address H-{}-{} {}/{}'.format(srcZone,ip,cidr,ip,cidr))
                        objGrSList.append('H-{}-{}'.format(ip,cidr))
                    else:
                        cidr = IPAddress(sources['ipSource']['mask']).netmask_bits()
                        finalConf.append('set security address-book {} address N-{}-{} {}/{}'.format(srcZone,ip,cidr,ip,cidr))
                        objGrSList.append('N-{}-{}'.format(ip,cidr))

            # Create destination address-set
            for i in objGrDList:
                finalConf.append('set security address-book {} address-set DG-{} address {}'.format(dstZone,inventory[0]['SNOW']['change'],i))
            DGLite = ('DG-{}'.format(inventory[0]['SNOW']['change']))
            objGrDList.clear()

            # Create source address-set
            for i in objGrSList:
                finalConf.append('set security address-book {} address-set SG-{} address {}'.format(srcZone,inventory[0]['SNOW']['change'],i))
            SGLite = ('SG-{}'.format(inventory[0]['SNOW']['change']))
            objGrSList.clear()

            # Create policies
            finalConf.append('set security policies from-zone TRUST to-zone UNTRUST policy {} match source-address {}'.format(inventory[0]['SNOW']['change'],SGLite))
            finalConf.append('set security policies from-zone TRUST to-zone UNTRUST policy {} match destination-address {}'.format(inventory[0]['SNOW']['change'],DGLite))


            # find existin application or create new
            for key in inventory:
                if 'protPortDestination' in key:
                    if key['protPortDestination']['protocol'] == 'tcp':
                        for port in key['protPortDestination']['ports']:
                            if port in appDicTCP.keys():
                                finalConf.append('set security policies from-zone {} to-zone {} policy {} match application {}'.format(srcZone,dstZone,inventory[0]['SNOW']['change'],appDicTCP[port]))
                            elif port not in appDicTCP.keys():
                                finalConf.append('set applications application TCP-{} protocol tcp destination-port {}'.format(port,port))
                                finalConf.append('set security policies from-zone {} to-zone {} policy {} match application TCP-{}'.format(srcZone,dstZone,inventory[0]['SNOW']['change'],port))

                    elif key['protPortDestination']['protocol'] == 'udp':
                        for port in key['protPortDestination']['ports']:
                            if port in appDicUDP.keys():
                                finalConf.append('set security policies from-zone {} to-zone {} policy {} match application {}'.format(srcZone,dstZone,inventory[0]['SNOW']['change'],appDicUDP[port]))
                            elif port not in appDicUDP.keys():
                                finalConf.append('set applications application UDP-{} protocol udp destination-port {}'.format(port,port))
                                finalConf.append('set security policies from-zone {} to-zone {} policy {} match application UDP-{}'.format(srcZone,dstZone,inventory[0]['SNOW']['change'],port))

            finalConf.append('set security policies from-zone {} to-zone {} policy {} then permit'.format(srcZone,dstZone,inventory[0]['SNOW']['change']))

            for config in finalConf:
                print(config)

            # Find the firewall
            for ip in dstAddress:
                for subnets in firewalls:
                    finder = ipaddress.ip_address(ip) in ipaddress.ip_network(subnets)
                    if finder is True:
                        device = firewalls[subnets]

            # config check
            try:
                secCheck = queryYesNo('Do you want proceed on {}?: '.format(device))
                # ssh session and firewall configuration
                if secCheck is True:
                    print('Please, provide login credentials:')
                    username = input("Username: ")
                    password = getpass.getpass()
                    hostIp = socket.gethostbyname(device)
                    print('\n' + ' CONFIGURATION IN PROGRESS '.center(50, '#') + '\n')
                    try:
                        sshSession = ConnectHandler(device_type='juniper', ip=hostIp, username=username, password=password)
                        sshSession.send_config_set(finalConf)
                        sshSession.commit()
                        finalConf.clear()
                        print('\n' + ' CONFIGURATION CHECK - START '.center(50, '#') + '\n')
                        print('show security address-book TRUST address-set SG-{}'.format(inventory[0]['SNOW']['change']))
                        print(sshSession.send_command('show security address-book TRUST address-set SG-{}'.format(inventory[0]['SNOW']['change'])))
                        print('show security address-book UNTRUST address-set DG-{}'.format(inventory[0]['SNOW']['change']))
                        print(sshSession.send_command('show security address-book UNTRUST address-set DG-{}'.format(inventory[0]['SNOW']['change'])))
                        print('show security policies from-zone TRUST to-zone UNTRUST policy {}'.format(inventory[0]['SNOW']['change']))
                        print(sshSession.send_command('show security policies from-zone TRUST to-zone UNTRUST policy {}'.format(inventory[0]['SNOW']['change'])))
                        print('\n' + ' CONFIGURATION CHECK - FINISH '.center(50, '#') + '\n')
                    except Exception:
                        print('Authentication failed! Please retry')
                        sys.exit(1)
                else:
                    pass
                    print('Bye from {}!'.format(device))

            except UnboundLocalError:
                print('\n' + ' DEVICE NOT FOUND IN FIREWALL LIST '.center(50, '#') + '\n')
                sys.exit(1)

if __name__ == '__main__':
    '''Juniper SRX policy configurator - Version 2.7 f180626'''
    # Catch CTRL-C
    try:
        main()
    except KeyboardInterrupt:
        print("\nCTRL-C caught, interrupting the script\n")
        sys.exit(1)

#180626
