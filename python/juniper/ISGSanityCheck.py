#!/usr/bin/env python3
import paramiko, time, smtplib, socket, os

username = 'username'
password = 'password'
device = ['device1.sw11.lab','device2.sw11.lab', 'device3.sw11.lab', 'device4.sw11.lab']
ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
max_buffer = 65535
FROM = 'lvrfrc87@gmail.com
TOALL = ['justfew@email.com', 'all@email.com']
TOIS = ['justfew@email.com']
smtpObj = smtplib.SMTP('10.0.0.1')
BODY = []

def splitBrain():
    ssh.connect(socket.gethostbyname('device1.sw11.lab'), username=username, password=password, look_for_keys=False, allow_agent=False)
    connection = ssh.invoke_shell()
    hostnamePrimary = connection.recv(max_buffer).decode()
    if '(M)' in hostnamePrimary:
        ssh.connect(socket.gethostbyname('device2.sw11.lab'), username=username, password=password, look_for_keys=False, allow_agent=False)
        connection = ssh.invoke_shell()
        hostnameBackup = connection.recv(max_buffer).decode()
        print0 = '#' * 10 + ' device1.sw11.lab-PRIMARY, device2.sw11.lab-BACKUP. Everything is OK ' + '#' * 10 + '\n'
        BODY.extend([print0, hostnamePrimary])
        if '(B)' in hostnameBackup:
            BODY.extend([hostnameBackup, '\n'])
        elif '(M)' in hostnameBackup:
            print2 = '#' * 10 + ' WARNING - Split brain in device1.sw11.lab/02 !!! - Raise as P3 to ISNetworks ' + '#' * 10 + '\n'
            BODY.extend([print2, hostnameBackup, '\n'])
    if '(B)' in hostnamePrimary:
        if '(M)' in hostnameBackup:
            print4 = '#' * 10 + ' NOTIFICATION - Primary failover: device1.sw11.lab --to--> device2.sw11.lab ' + '#' * 10 + '\n'

    ssh.connect(socket.gethostbyname('device3.sw11.lab'), username=username, password=password, look_for_keys=False, allow_agent=False)
    connection = ssh.invoke_shell()
    hostnamePrimary = connection.recv(max_buffer).decode()
    if '(M)' in hostnamePrimary:
        ssh.connect(socket.gethostbyname('device4.sw11.lab'), username=username, password=password, look_for_keys=False, allow_agent=False)
        connection = ssh.invoke_shell()
        hostnameBackup = connection.recv(max_buffer).decode()
        print5 = '#' * 10 + ' device3.sw11.lab-PRIMARY, device4.sw11.lab-BACKUP. Everything is OK ' + '#' * 10 + '\n'
        BODY.extend([print5, hostnamePrimary])
        if '(B)' in hostnameBackup:
            BODY.extend([hostnameBackup, '\n'])
        elif '(M)' in hostnameBackup:
            print6 = '#' * 10 + ' WARNING - Split brain in device3.sw11.lab/02 !!! - Raise as P3 to ISNetworks ' + '#' * 10 + '\n'
            BODY.extend([print6, hostnameBackup, '\n'])
    if '(B)' in hostnamePrimary:
        if '(M)' in hostnameBackup:
            print7 = '#' * 10 + ' NOTIFICATION - Primary failover: IE"EXTFW01 --to--> IE"EXTFW02 ' + '#' * 10 + '\n'
            BODY.extend([print7, hostnameBackup, '\n'])

def configSync():
    for firewalls in devices:
        ssh.connect(socket.gethostbyname(firewalls), username=username, password=password, look_for_keys=False, allow_agent=False)
        connection = ssh.invoke_shell()
        connection.send('exec nsrp sync global-config check-sum\n')
        time.sleep(1)
        haSync = connection.recv(max_buffer).decode()
        if 'Warning: configuration out of sync' in haSync:
            print8 = '#' * 10 + ' WARNING - configuration out of sync ' + '#' * 10 + '\n'
            BODY.extend([print8, haSync])
        elif 'configuration in sync' in haSync:
            print9 = '#' * 10 + ' Config in sync for {} - Everything is OK '.format(firewalls) + '#' * 10 + '\n'
            BODY.extend([print9, haSync])

def writeBody():
    with open('bodyEmail.txt', 'w') as bodyEmail:
        for i in BODY:
            bodyEmail.write(i + '\n')
    with open('bodyEmail.txt', 'r') as email:
        return email.read()

def sendEmail(body):
    SUBJECT = 'Sanity check report for device(1|2|3|4).sw11.lab'
    smtpObj.sendmail(FROM, TOIS, 'Subject: {}\n\n {}:'.format(SUBJECT,body))
    os.remove('bodyEmail.txt')

def main():
    splitBrain()
    configSync()
    sendEmail(writeBody())

if __name__ == '__main__':
    print('#' * 10 + ' SCRIPT IS RUNNING ' + '#' * 10 + '\n')
    main()

#f20180418
