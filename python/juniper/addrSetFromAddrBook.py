#!/usr/bin/env python
import paramiko, time, re, getpass
from clearBuffer import clearBuffer

username = raw_input('Username: ')
password = getpass.getpass('Password: ')
max_buffer = 65535

def connection():
  while True:
    firewall = raw_input('Please, select PRMFW01 or PRMFW02 > ')
    if firewall == 'PRMFW01':
      device = '10.36.0.1'
      print('Connected to PRMFW01:10.36.0.1')
      return device
   elif firewall == 'PRMFW02':
      device = '10.37.0.2'
      print('Connected to PRMFW02:10.37.0.2')
      return device
    else:
      print "Wrong device, options available PRMFW01 or PRMFW02"

def shell(device):
  ssh = paramiko.SSHClient()
  ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
  ssh.connect(device, username=username, password=password, look_for_keys=False, allow_agent=False)
  connection = ssh.invoke_shell()
  return connection

def show(connection):
  connection.send('\n')
  connection.send('show configuration security policies | display set | no-more\n')
  time.sleep(5)
  output_0 = connection.recv(max_buffer)
  print output_0
  from_zone = raw_input("from zone:> ")
  to_zone = raw_input("to zone:> ")
  policy_name = raw_input("policy name:> ")
  connection.send('\n')
  connection.send('show configuration security policies from-zone ' + from_zone + ' to-zone ' + to_zone + ' policy ' + policy_name + '| display set | no-more\n')
  time.sleep(3)
  output_1 = connection.recv(max_buffer)
  print output_1
  s_add = re.findall(r'source-address[ ]+([0-9.\/]+)', output_1)
  d_addr = re.findall(r'destination-address[ ]+([0-9.\/]+)', output_1)
  a_book_set = raw_input("give me the address-book set name:> ")

  while True:
    a_b_type = raw_input("do you want create an address-set for source-address or destination-address ?:> ")
    if a_b_type == "source-address":
      for i in s_add:
        print "set security zones security-zone %s address-book address-set %s address %s" % (from_zone, a_book_set, i)
      break
    elif a_b_type == "destination-address":
      for i in d_addr:
        print "set security zones security-zone %s address-book address-set %s address %s" % (to_zone, a_book_set, i)
      break
    else:
      print "Please prompt source-address or destination-address"

  if a_b_type == "source-address":
    for i in s_add:
      print "delete security policies from-zone %s to-zone %s policy %s match source-address %s" % (from_zone, to_zone, policy_name, i)
      print "set security policies from-zone %s to-zone %s policy %s match source-address %s" % (from_zone, to_zone, policy_name, a_book_set)
    print 'commit comment "+address-set %s"' % a_book_set
  elif a_b_type == "destination-address":
    for i in d_addr:
      print "delete security policies from-zone %s to-zone %s policy %s match destination-address %s" % (from_zone, to_zone, policy_name, i)
      print "set security policies from-zone %s to-zone %s policy %s match destination-address %s" % (from_zone, to_zone, policy_name, a_book_set)
    print 'commit comment "+address-set %s"' % a_book_set
  else:
    print "Try Again"
  print 'SSH session closed'
  connection.close()

def main():
  show(shell(connection()))

main()
#f20180301
