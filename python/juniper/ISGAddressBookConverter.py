#!/usr/bin/env python3

import yaml
from netaddr import IPAddress

aBook = yaml.load(open('addressBookISGTrust.yaml', 'rb'))

ipAddr = []
subnet = []

def main():
    for ip in aBook['myAddressBook']['address']:
        ipAddr.append(ip)
    for mask in aBook['myAddressBook']['mask']:
        subnet.append(mask)

    for i,j in zip(ipAddr,subnet):
        if j == '255.255.255.255':
            print('set security address-book TRUST address H-{}-{} {}/{}'.format(i,IPAddress(j).netmask_bits(),i,IPAddress(j).netmask_bits()))
        else:
            print('set security address-book TRUST address N-{}-{} {}/{}'.format(i,IPAddress(j).netmask_bits(),i,IPAddress(j).netmask_bits()))


if __name__ == '__main__':
    main()
