# networkAutomation

My dirty python scripts to make a network engineer's life easier (plus other stuff). In each folder you can find the python script an an output example from the same script. Please, do contribute as much as you can!

### python/cisco

*aclConfigurator*

INFO: Porviding a YAML file as input for source IP, destination IP, protocol and destination port this script is able to build ACL configuration an push-it automatically to the right device (based on a inventory - YAML format)

*aclAudit*

INFO: Let' s assume you have a huge ACL and a some point in your life you decided to clean up that ACL from entries without any matches. Instead to check it manually and one by one, you can run this script that will give you the final config file to copy and paste on your device. HOW-IT-WORKS: the script goes through the ACL list that you copy and pasted on a file (output of: show ip access-list extended MY-ACL) and will check those entries without any match or matches. Check the example file for better understanding of the script purpose

*ipsecConfigAttributes.py*

INFO: This script help you to identify all crypto-map conig related (crypto-map, ACLs, nat).
You will be asked to insert the third party name you are looking for (this must match the ACL name under crypto-map) as well as the crypto-map number you want see. Check the example file for better understanding of the script purpose

*logChecker.py*

INFO: Easy script to grep your log file for one or more keyword using and/or statements. You can scan one or more files adding file name on list (fileList) as well as add new terms (term1, term2, term3)

### python/juniper

*addrSetFromAddrBook.py*

INFO: Script to create address-set from address book on SRX firewall. In case you have a bunch of source-address or destination-address in your policy and you want "summarize" them in a single address set. Just give the info required from the script and you will have a copy and paste config. Check the example file for better understanding of the script purpose

*dstNatPool.py*

INFO: (very) quick and dirty script to grap all public IPs dedicated to DNAT rules. Using Netmiko instead Paramiko. Check the example file for better understanding of the script purpose

*ISGAddressBookConverter.py*

INFO: The purpose of this script was to grap all address book in ScreenOS ISG and convert them in SRX address book format. I said "was" because I found later that juniper provided a config converter toll from ScreenOS to SRX so, this script became quite useless

*ISGConfigSyncCheck.py*

INFO: Script to check if 2 ISGs nodes in HA have config sync between them. Report sent via e-email

*ISGSanityCheck.py*

INFO: Similar to the above plus check for possible split brain between nodes. Report sent via e-email

### python/vendorNeutral

*maAddressFinder*

INFO: Find a MAC address across mutlivendor switches. Running in multithread so, no difference in time between 10 and 100 switches. You can pass MAC in whatever format you like: hh-hh-hh-hh, hhhh.hhhh.hhhh, hh:hh:hh:hh either upper or lower case

### python/jinja

INFO: Some jinja template for firewall IPSEC configuration (Cisco ASA and Juniper SRX)


### Ansible

#### Linux & Networking_Multivendor

INFO: bunch of playbooks for different networking vendors (JunOS, EoS, ASA, IOS, Nexus, etc.) for SNMP and RADIUS config update. Some of them are based on roles and include_task. Implementation NAPALM on Ansible is on-going

### srv/salt

INFO: How to manage multi-vendor netowrking devices via single command line. Integration between SaltStack and NAPALM. Including some basic Jinja templates, YANG file for openconfig

### CCIE

My personal notes about my on-goin CCIE R&S Written experience

### Docker Hub

https://hub.docker.com/u/federico87/

### PyPi Repo

https://pypi.org/user/Federico87/
