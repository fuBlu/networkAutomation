No.     Time           Source                Destination           Protocol Length Info
    166 337.796460000  3.3.3.3               224.0.1.39            Auto-RP  62     RP announcement (v1 or 1+) for 1 RP

Frame 166: 62 bytes on wire (496 bits), 62 bytes captured (496 bits) on interface 0
Ethernet II, Src: ca:02:44:01:00:1c (ca:02:44:01:00:1c), Dst: IPv4mcast_01:27 (01:00:5e:00:01:27)
Internet Protocol Version 4, Src: 3.3.3.3 (3.3.3.3), Dst: 224.0.1.39 (224.0.1.39)
User Datagram Protocol, Src Port: 496 (496), Dst Port: 496 (496)
Cisco Auto-RP
    Version: 1 or 1+, Packet type: RP announcement
    RP count: 1
    Holdtime: 181 seconds
    Reserved: 0x0
    RP 3.3.3.3: 1 group
        RP address: 3.3.3.3 (3.3.3.3)
        .... ..11 = Version: Dual version 1 and 2 (3)
        Number of groups this RP maps to: 1
        Group 238.0.0.0/8 (Positive group prefix)
            .... ...0 = Sign: Positive group prefix (0)
            Mask length: 8
            Prefix: 238.0.0.0 (238.0.0.0)

No.     Time           Source                Destination           Protocol Length Info
    180 363.513561000  4.4.4.4               224.0.1.39            Auto-RP  60     RP announcement (v1 or 1+) for 1 RP

Frame 180: 60 bytes on wire (480 bits), 60 bytes captured (480 bits) on interface 0
Ethernet II, Src: ca:07:79:0d:00:1c (ca:07:79:0d:00:1c), Dst: IPv4mcast_01:27 (01:00:5e:00:01:27)
Internet Protocol Version 4, Src: 4.4.4.4 (4.4.4.4), Dst: 224.0.1.39 (224.0.1.39)
User Datagram Protocol, Src Port: 496 (496), Dst Port: 496 (496)
Cisco Auto-RP
    Version: 1 or 1+, Packet type: RP announcement
    RP count: 1
    Holdtime: 181 seconds
    Reserved: 0x0
    RP 4.4.4.4: 0 groups
        RP address: 4.4.4.4 (4.4.4.4)
        .... ..11 = Version: Dual version 1 and 2 (3)
        Number of groups this RP maps to: 0

No.     Time           Source                Destination           Protocol Length Info
    205 411.048881000  3.3.3.3               224.0.1.39            Auto-RP  62     RP announcement (v1 or 1+) for 1 RP

Frame 205: 62 bytes on wire (496 bits), 62 bytes captured (496 bits) on interface 0
Ethernet II, Src: ca:02:44:01:00:1c (ca:02:44:01:00:1c), Dst: IPv4mcast_01:27 (01:00:5e:00:01:27)
Internet Protocol Version 4, Src: 3.3.3.3 (3.3.3.3), Dst: 224.0.1.39 (224.0.1.39)
User Datagram Protocol, Src Port: 496 (496), Dst Port: 496 (496)
Cisco Auto-RP
    Version: 1 or 1+, Packet type: RP announcement
    RP count: 1
    Holdtime: 181 seconds
    Reserved: 0x0
    RP 3.3.3.3: 1 group
        RP address: 3.3.3.3 (3.3.3.3)
        .... ..11 = Version: Dual version 1 and 2 (3)
        Number of groups this RP maps to: 1
        Group 238.0.0.0/8 (Positive group prefix)
            .... ...0 = Sign: Positive group prefix (0)
            Mask length: 8
            Prefix: 238.0.0.0 (238.0.0.0)

No.     Time           Source                Destination           Protocol Length Info
    221 437.365114000  4.4.4.4               224.0.1.39            Auto-RP  60     RP announcement (v1 or 1+) for 1 RP

Frame 221: 60 bytes on wire (480 bits), 60 bytes captured (480 bits) on interface 0
Ethernet II, Src: ca:07:79:0d:00:1c (ca:07:79:0d:00:1c), Dst: IPv4mcast_01:27 (01:00:5e:00:01:27)
Internet Protocol Version 4, Src: 4.4.4.4 (4.4.4.4), Dst: 224.0.1.39 (224.0.1.39)
User Datagram Protocol, Src Port: 496 (496), Dst Port: 496 (496)
Cisco Auto-RP
    Version: 1 or 1+, Packet type: RP announcement
    RP count: 1
    Holdtime: 181 seconds
    Reserved: 0x0
    RP 4.4.4.4: 0 groups
        RP address: 4.4.4.4 (4.4.4.4)
        .... ..11 = Version: Dual version 1 and 2 (3)
        Number of groups this RP maps to: 0

No.     Time           Source                Destination           Protocol Length Info
    247 486.104985000  3.3.3.3               224.0.1.39            Auto-RP  62     RP announcement (v1 or 1+) for 1 RP

Frame 247: 62 bytes on wire (496 bits), 62 bytes captured (496 bits) on interface 0
Ethernet II, Src: ca:02:44:01:00:1c (ca:02:44:01:00:1c), Dst: IPv4mcast_01:27 (01:00:5e:00:01:27)
Internet Protocol Version 4, Src: 3.3.3.3 (3.3.3.3), Dst: 224.0.1.39 (224.0.1.39)
User Datagram Protocol, Src Port: 496 (496), Dst Port: 496 (496)
Cisco Auto-RP
    Version: 1 or 1+, Packet type: RP announcement
    RP count: 1
    Holdtime: 181 seconds
    Reserved: 0x0
    RP 3.3.3.3: 1 group
        RP address: 3.3.3.3 (3.3.3.3)
        .... ..11 = Version: Dual version 1 and 2 (3)
        Number of groups this RP maps to: 1
        Group 238.0.0.0/8 (Positive group prefix)
            .... ...0 = Sign: Positive group prefix (0)
            Mask length: 8
            Prefix: 238.0.0.0 (238.0.0.0)

No.     Time           Source                Destination           Protocol Length Info
    248 486.125008000  3.3.3.3               224.0.1.39            Auto-RP  62     RP announcement (v1 or 1+) for 1 RP

Frame 248: 62 bytes on wire (496 bits), 62 bytes captured (496 bits) on interface 0
Ethernet II, Src: ca:04:44:2f:00:1c (ca:04:44:2f:00:1c), Dst: IPv4mcast_01:27 (01:00:5e:00:01:27)
Internet Protocol Version 4, Src: 3.3.3.3 (3.3.3.3), Dst: 224.0.1.39 (224.0.1.39)
User Datagram Protocol, Src Port: 496 (496), Dst Port: 496 (496)
Cisco Auto-RP
    Version: 1 or 1+, Packet type: RP announcement
    RP count: 1
    Holdtime: 181 seconds
    Reserved: 0x0
    RP 3.3.3.3: 1 group
        RP address: 3.3.3.3 (3.3.3.3)
        .... ..11 = Version: Dual version 1 and 2 (3)
        Number of groups this RP maps to: 1
        Group 238.0.0.0/8 (Positive group prefix)
            .... ...0 = Sign: Positive group prefix (0)
            Mask length: 8
            Prefix: 238.0.0.0 (238.0.0.0)

No.     Time           Source                Destination           Protocol Length Info
    250 486.145366000  5.5.5.5               224.0.1.40            Auto-RP  62     RP mapping (v1 or 1+) for 1 RP

Frame 250: 62 bytes on wire (496 bits), 62 bytes captured (496 bits) on interface 0
Ethernet II, Src: ca:04:44:2f:00:1c (ca:04:44:2f:00:1c), Dst: IPv4mcast_01:28 (01:00:5e:00:01:28)
Internet Protocol Version 4, Src: 5.5.5.5 (5.5.5.5), Dst: 224.0.1.40 (224.0.1.40)
User Datagram Protocol, Src Port: 496 (496), Dst Port: 496 (496)
Cisco Auto-RP
    Version: 1 or 1+, Packet type: RP mapping
    RP count: 1
    Holdtime: 181 seconds
    Reserved: 0x0
    RP 3.3.3.3: 1 group
        RP address: 3.3.3.3 (3.3.3.3)
        .... ..11 = Version: Dual version 1 and 2 (3)
        Number of groups this RP maps to: 1
        Group 238.0.0.0/8 (Positive group prefix)
            .... ...0 = Sign: Positive group prefix (0)
            Mask length: 8
            Prefix: 238.0.0.0 (238.0.0.0)
