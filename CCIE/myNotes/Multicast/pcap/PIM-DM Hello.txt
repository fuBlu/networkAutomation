No.     Time           Source                Destination           Protocol Length Info
      3 10.275597000   10.1.1.1              224.0.0.13            PIMv2    72     Hello

Frame 3: 72 bytes on wire (576 bits), 72 bytes captured (576 bits) on interface 0
Ethernet II, Src: ca:01:46:d5:00:08 (ca:01:46:d5:00:08), Dst: IPv4mcast_0d (01:00:5e:00:00:0d)
Internet Protocol Version 4, Src: 10.1.1.1 (10.1.1.1), Dst: 224.0.0.13 (224.0.0.13)
Protocol Independent Multicast
    0010 .... = Version: 2
    .... 0000 = Type: Hello (0)
    Reserved byte(s): 00
    Checksum: 0xdce6 [correct]
    PIM options: 5
        Option 1: Hold Time: 105s 
            Type: 1
            Length: 2
            Holdtime: 105s 
        Option 20: Generation ID: 1015924457
            Type: 20
            Length: 4
            Generation ID: 1015924457
        Option 19: DR Priority: 1
            Type: 19
            Length: 4
            DR Priority: 1
        Option 21: State Refresh Capable: Version = 1, Interval = 0s
            Type: 21
            Length: 4
            Version: 1
            Interval: 0
            Reserved: 0
        Option 65004: Unknown: 65004
            Type: 65004
            Length: 0

No.     Time           Source                Destination           Protocol Length Info
      4 10.284335000   10.1.1.2              224.0.0.13            PIMv2    72     Hello

Frame 4: 72 bytes on wire (576 bits), 72 bytes captured (576 bits) on interface 0
Ethernet II, Src: ca:02:48:c9:00:08 (ca:02:48:c9:00:08), Dst: IPv4mcast_0d (01:00:5e:00:00:0d)
Internet Protocol Version 4, Src: 10.1.1.2 (10.1.1.2), Dst: 224.0.0.13 (224.0.0.13)
Protocol Independent Multicast
    0010 .... = Version: 2
    .... 0000 = Type: Hello (0)
    Reserved byte(s): 00
    Checksum: 0x913d [correct]
    PIM options: 5
        Option 1: Hold Time: 105s 
            Type: 1
            Length: 2
            Holdtime: 105s 
        Option 20: Generation ID: 973280541
            Type: 20
            Length: 4
            Generation ID: 973280541
        Option 19: DR Priority: 1
            Type: 19
            Length: 4
            DR Priority: 1
        Option 21: State Refresh Capable: Version = 1, Interval = 0s
            Type: 21
            Length: 4
            Version: 1
            Interval: 0
            Reserved: 0
        Option 65004: Unknown: 65004
            Type: 65004
            Length: 0
            


# PIM-DM Designated Router
#
# In addition to establishing PIM Neighbor adjacencies, PIM Hello messages are also used to elect the Designated Router (DR) for a multi-access network. 
# PIM routers make note (via PIM Hello messages) of the router on the network with the highest IP address. This PIM router becomes the DR for the network.
#
# NOTE The DR is primarily used in sparse mode networks and has little meaning in dense mode networks. The only exception to this rule is when IGMPv1 is in use on an interface. 
# In this case, the PIM-DR also functions as the IGMP Query Router since IGMPv1 does not have a Query Router election mechanism

