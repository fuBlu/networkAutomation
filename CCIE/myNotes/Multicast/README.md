#### SIX BASIC REQUIREMENTS FOR SUPPORTING MULTICAST ACROSS A ROUTED NETWORK

1. A designate range of L3 adresses to be assigned to mulicast application.
2. IP multicast addrss must be used only as destination IP (source IP will be unicast IP from source server/application).
   Multicast IP address doesnt identify a final host but rather signifies that the paclet is carrying multicast traffic
   for a specific application.
3. Multicast application (running same L3 used from multicast server as destination multicast IP) must be installed in a host. 
   This is referred to *join a group*
4. A host running a multicast application, must be able to calculate L2 multicast address and assign it to its NIC
5. A host must dynammical support a mechanism to inform L3 devices that it would like to recieve multicast traffic 
   for a specific group (IGMP, IGMP Snooping)
6. There must be a multicast routing protocol that allows mutlicast routers to forward multicast traffic.

#### MULTICAST IP ADRESSES

ClassD --> from 224.0.0.0 to 239.255.255.255. The first 4 bits are alway 1110 

#### WELL KNOWN MULTICAST IP ADRESSES
#### Multicast adresse for Permanent Groups - Range 1
From *224.0.0.0* to *224.0.0.255*

First range is used for packets that should not forwarded by routers.
224.0.0.1 	  The All Hosts multicast

224.0.0.2 	  The All Routers multicast

224.0.0.4 	  Distance Vector Multicast Routing Protocol (DVMRP)

224.0.0.5     The Open Shortest Path First (OSPF)

224.0.0.6 	  The OSPF All Designated Routers (DR)

224.0.0.9 	  The Routing Information Protocol (RIP) version 2

224.0.0.10    The Enhanced Interior Gateway Routing Protocol (EIGRP)

224.0.0.13    Protocol Independent Multicast (PIM) Version 2

224.0.0.18    Virtual Router Redundancy Protocol (VRRP)

224.0.0.22 	  Internet Group Management Protocol (IGMP) version 3

224.0.0.102   Hot Standby Router Protocol version 2 (HSRPv2) / Gateway Load Balancing Protocol (GLBP)

224.0.0.139   Cisco RP Announce

224.0.0.140   Cisco RP Discovery

#### Multicast adresse for Permanent Groups - Range 2
Second range is used for packets that should forwarded by routers.

From *224.0.1.0* to *224.0.1.255*

#### Source-Specifc Multicast Application and Protocols
From *232.0.0.0* to *232.255.255.255*

Used from host to select the source for the multicast group

#### GLOP Addressing
From *233.0.0.0* to *233.255.255.255*

Range assigned from IANA. Just 256 IP can be assigned per ASN. 
Example of assignemt:

ASN 5663 --> binary 0001011000011111, first 8 bits (00010110) are equal to 22 (in decimal notaion); second 8 bits (00011111) are equal to 31 (in decimal notaion). IP address range assigned from 233.22.31.0 to 233.22.31.255

#### Private Multicast Domains
From *239.0.0.0* to *239.255.255.255*

Like IP unicast range in RFC1918

#### Transient Group
*All other the remaining IPs*

Used for global multicast application. Must be shared across the entire internet. They must be dynamically allocated when needed and must be relased when no longer in use.


#### MAPPING IP MULTICAST ADRESSES TO MAC ADRESSES
Assign a L3 multicast address to a multicast group (application) automatically generates a L2 multicast address

IP example: 228.10.24.5 (binary 11100100.00001010.00011000.00000101)

First 24 bits are always: 0x01-00-5e

25th bit is always: 0x0

Last 23 bits correspond to the last 23 bits from IP multicast address and converted from to hexadecimal:0xa-18-05

MAC:01-00-5e-0a-18-05

#### IGMPv2

IGMPv2 as TTL value of 1

##### Query a group

Router: sends IGMPv2 *Host Membership Query* every 60 seconds (destIP: 224.0.0.1 destMAC: 01-00-5e-00-00-01)

##### Join a group

Host: sends *Host Membership Report* when receives Host Membership Query from router (sollicited) or when it joins a group 
      (unsolicited)
      
Host: when host receives Host Membership Query, it picks up a random number between 0 and MTR (Maximum Response Time - 0.1 to
      25.5) - this time is called *Query response Interval*. When the timer expires, the host will send the Host Membership 
      Report but only if has not already heard another host send a report for its group. This is call *Report Suppresion*
      
##### Leave a group

IGMv1 vs. IGMv2 == 3 minutes vs. 3 seconds

Host: sends *Leave Group Message* (destIP:224/0.0.2)

Router: after received a Leave Group Message, the router sends a *Group-Specific Query* as destIP the group IP (i.e. 226.1.1.1). 
        This step has been done in order to verify if there are other hosts connected to that segment of LAN that they want 
        still receive traffic for the multicast group (226.1.1.1)

Host-2: receives Group-Specific Query and it respons with a IGMv2 Membership Report to inform the router on the subnet that 
        it is still a member of group. 
        
N.B. Report Suppression mechanism is also valid for Leave Group stage

Router: if doesn't receive any Host Memebr Report withn a MTR of 10 (1 sec. default value) - called *Last Member Query Interval* 
        the router send again a Group-Specific Query for a number of time repeated on Last Member Query Count (default value is 
        2 so latency is 3 seconds compare to 3 minutes for IGMPv1)

##### Querier

If 2 or more router are connected to the same subnet, the router with the lowest interface IP will take the priority and will start to send Host membership Query. The other(s) router(s) remain in stan-by. The stand-by router will take the ownership when the master router doesn't send a query for 2 consecutive *Query Interval* ( 2x *Other Query Interval*(125 secs)) plus half of one *Query respons Interval* (def. 10 secs /2 = 5 secs). Tot. 255 secs.
        
#### IGMPv3

It provides DOS protection via *Source-Specific Multicast* 

Host: sends IGMPv3 Memebership Report destIP: 224.0.0.22 with a note "Source-INCLUDE-x.x.x.x" which means "I would like to
      receive multicast group 226.1.1.1 only if the group traffic is coming from x.x.x.x"

#### IGMPv1 and IGPMv2 Interoperability

##### IGMPv2 Host and IGMPv1 Router

IGMPv2 hosts determine wheter the qerying router is an IGMPv1 or IGMPv2 based on MTR field on Query message(IGMPv1 = 0, IGMPv2
!= 0). In that case, the host stops to send IGMPv2 ans starts a 400 seconds Version 1 *Router Present Timeout*

##### IGMPv1 Host and IGMPv2 Router

Router can understand if host is running IGMPv1 or v2 from the Report messages received (0x12 vs. 0x16). Router sends IGMPv2 queries and host can understand them because IGMPv1 and IGMPv2 queries are similar (except for the second octect which is ignored from IGMPv1). When IGMPv1 router receives IGMPv2 Report, the router ignores the Leave Message and Group-Specific Query (that happens because IGMPv1 host does not have understandi of those messages, so the router would erroneously conlcude that nobody wants recevie traffic for that group).
When IGMPv2 router receives a IGMPv1 Report, the router must send an IGMPv1-host-present countdow timer (180sec in IGMPv1, 260 secs in IGMPv2). If router doesn' t receives report whitin that time, it conlcudes that hosts dont want longer receive traffic for that multicast group

#### IGMP Snooping
 
 If a switch receive a multicast pachet, by default it forwards out all ports belonging to the VLAN where the packet has been received. This happen because the switch will never learn the destMAC 01.00.5e.0x.xx.xx (that MAC is always used as destMAC). To reduce this flooding, the switch analyze the IGMP packets exchange between router and hosts.
 
 ##### IGMP Snooping - Join a group
 
 Switch-Router: to detect where the router is connected, the switch listens for multicast routing traffic (OSPF, VRRP, IGMP 
                General Query, etc.). The port is added to CAM with GDA
                
 Switch-Host: the switch detecet where a host is connected via IGMP Join message. After receiving the IGMP message, the switch 
              add the port to CAM for that specific GDA for Non-IGMP traffic. Also it creates a port 0 where redirect all 
              L3-IGMP traffic for that GDA
 
Once the switch has the CAM populated for the Router and all the Hosts, it starts to forward multicaf traffic for specific group to those ports only. N.B. Since the switch intercept the IGMP Report, the Report Suppression mechanism breakes (the switch will receive a report from each host but will forward just one to the router)

 ##### IGMP Snooping - Leave a group
 
 Switch-Host: switch receives IGMP Leave message from host. The switch replies with IGMP General Query to make sure not other
              hosts are connected to that segment. Since the switch acts like a "proxy" the router doesn't even know that an
              host has left the group.
              
 Switch-Router: when the switch realizes that the last host has left the group, it sends a IGMP leave 
                to the router that replies with IGMP Group-Specific Query. Since no host are longer connected to the group, 
                the router won't recieve any respons and it will stop to send multicast traffic for that group
              
### MULTICAST ROUTING BASICS

#### DENSE MODE

Dense Mode believes that all subnets require multicast traffic, therefore it forwards multicast out to all its interfaces with the exception of:

- Interface where the multicast traffic is coming from
- A downstream router doesn't have any active host for that group
- The router dosen't have host connected that have joined that group

Dense Mode implements *RPF* to preventing loop when forwarding multicast traffic. RPF look at the source IP of the multicast packet (i.e. unicast: 10.1.1.10). If the route that matches the source IP lists an outgoing interface that is the actual interface where the multicast traffic was received, the RPF check pass, otherwise it fails (shortest path wins).

##### Multicast Scoping

TTL scoping: Each interface is configured with a TTL value. The router compare the TTL value on the received packet (decreeased by one) with the onterface TTL configured. If the packet TTL is bigger or equal to interface TTL, the packet is forwarded. 
If the packet TTL is lower than interface TTL, the packet is dropped.

Administrative Scoping: ACLs for private multicast IPs. They don't allow those subnets to be forwarder outside the domain

#### Protocol Indipendent Multicast (PIM) Dense Mode

PIM uses unicast IP routing table for RPF check.
PIMv2 (the current) forms adjacensies via *Hello* messages every 30 seconds (by default) on every interface where PIM is confiugred. PIMv2 uses IP protocol 103 and destIP 224.0.0.13 (All-PIM Routers). *Hold* time is 3xHello (90 secs by default)

##### Source-Based Distribution Trees

PIM-DM *(S,G)* notation refers to a particular *Shortest Path Tree (SPT)* where S is the source adress and G is the multicast group address.

##### Prune Message

In case RPF fails or no host are listening (anymore) for a particular multicast group, the router sends a *Prune Message* for that particular (S,G)SPT to its upstream neighbour. This causes the upstream router to remove the link for a particular (S,G)SPT on which the prune message has been received.
Once a prune message is received, the router starts a timer of 3 minutes. Once the timere expires, if the router doesn't receive any *Refresh* message from its upstream neighbour, it starts again to flood traffic

##### Graft Message

When a router receives a IGMP Join from a host, it sends a *Graft Message* to its upstream neighbour to reset Prune timer (if previously pruned) and put the link back in forwarding state for a particular (S,G)SPT. The upstream router replies with *Graft/ACK*

##### LAN-Specific Issues

Prune Override: In multiaccess network, a router that receives a Prune Message, wait for 3 seconds before to remove the link from a specific (S,G)SPT. If the router receives a Join message within those 3 seconds, the router ovverrides the prune

Assert Message: When more than one router is attahced to the same LAN. The Router with (in order in case of tie) 

- lowest AD for source IP of multicast group 

- lowest metric for source IP of multicast group

- highest IP on the LAN

will start to forward forward packets for a specific (S,G)SPT

Designate router (IGMPv1): Same function as in OSPF. Router with highest IP wins. In IGMPv2 there there is a Querier

N.B. Assert: forward traffic; Querier: handle IGMP process.

#### SPARSE MODE

PIM-SM assumes that no hosts want to receive multicast traffic untill they specifically ask to receive it.
PIM-SM has the following similarities compared with PIM-DM:

1.PIM neighbour discovery via Hello (every 60 secs)
2.RPF interface
3.Election DR for IGMPv1
4.Prune Override 
5.Assert Message

##### Source Packets to RP

S1: sends multicast traffic to router connected on LAN for 228.8.8.8

R1: reacts sending *PIM Register* messages to RP (configured via *ip pim rpm-address x.x.x.x*) for (s,G). Register are unicast messages with destination RP's IP and they encapsulate multicast packets received from the source (S1). RP recognizes itslef via *ip pim rpm-address x.x.x.x* where x.x.x.x is its IP address

RP: since RP doesn't have yet any (S,G) or (*,G) for 228.8.8.8 address, it sends backt to R1 a *Register-Stop*

R1: keep sending Register until it receives a Register-Stop from RP. At that point R1 starts a 1 minute timer. 5 seconds before  the timer expires, R1 sens another Register with Null-Registr flag and no multicast packet encapsulated

RP: if doesn't want yet traffic for 228.8.8.8, it send back to R1 a Register-Stop and R1 restart the timer. Else RP doesn' t reply to the last R1's register and R1 starts to send multicas traffic after the timer is expired

##### Join a Shared Tree (Root Path Tree - RPT)

PIM-SM routers collectively create a RPT by sending PIM Join messagrs toward the RP. This happen under 2 conditions:

1- PIM-SM router receives a PIM Join message on any interface other than that one used to route packets to RP
2- PIM-SM router receives a Memebership Report form a connected host

H1: Sending Join mmessage for group 228.8.8.8

R4 (connected to H1): sends a Jon Message to RP via Shared Tree and puts eth0 interface (connected to H1) in forwarding status.

R5 (sitting between R4 and RP): receives Join message from R4 and put Serial1 interface in forwarding mode. It also forwards the Join message to RP

RP: put Serial0 in forwarding state for (*,228.8.8.8) shared tree (R4-R5-RP).

The '*' means "any source": RP uses the Shared Tree regardless of the source.


##### Source Registartion Process

RP: receives a Register message for an active multicast group (that is, a group for which it has received a Join message). RP decpasulates unicast Register and forward multicast traffic along the RPT. In the meantime it complete the Registration process with R1

RP: since it received traffic for specific (S,G) (10.1.1.10,228.8.8.8) RP asks to join it via PIM-SM Join message toward the source 10.1.1.10

R1, R2 (sitting between R1 and RP): since they received Join for (S,G), they start to forward multicast to RP. R1 Still sending Register with multicast encapsuleted on it

RP: Sends Register-Stop to R1 and R1 stops sending encapsulated multicast traffic

As long as the downstream routers will send along the RPT the Join messages every 60 seconds for (*,G) the Prune timer will be reset on upstream router. 
As long as the Host will reply to IGMP General Query (every 60 seconds) with IGMP Report/Join, the gateway router will reset the prune timer.

SPT (SpurcePathTree) - Where the traffic is pulled from RP
RPT (RootPathTree or SharedTree) - Where the traffic is pushed from RP

##### Source-Specific Path Switchover

Cisco routers by default switch over from the RTP to Source specif SPT after they receive the first packet from the shared tree

A router check its routing table and it finds a better path for a specific source of a (S,G) and it starts to send Join message out the prefered interface. Once the Join messages reach the DR, the router star to send multicast traffic via the new path.
In order to inform the RP and upstream routers to stop to send traffic via the old path, the router will send a Prune  message for specific (S,G) through the RPT. The prune will set a bit called RP-tree bit.

#### Auto-RP

RP IPs are autamitally learned from all PIM routers. Provide redundancy in case one RP fails and load-balancing per group between RPs (one RP is owner for on (S,G) another RP is owner of another (S,G))

Learning process is devided in 2 steps:

- RP sends *RP-Announce* at well known address *224.0.1.39* every 60 seconds. RP-Anounce incluede the multicast groups which RP is responsible for. i.e (10.1.10.3,224.0.1.39) where 10.1.10.3 is RPs loopback interface injected via IGP

- A *Mapping Agent* (that can be a second router or the RP itself), listening for 224.0.1.39. Once received a RP-Announcement, the Mapping Agent creates a mapping between  multicast group and RP i.e. (224.0.0.0/4,10.1.10.3) - that means "all multicast traffic to RP with 10.1.10.3 IP". AFter the mapping is created, the Mapping Agent send multicst *RP-Discovery* at 224.0.1.40. All PIM router listengin for that IP will receive a mapping multicast-group/RP. In case of tie between 2 RPs, the Mapping Agent prefr the one with highest IP

Chicken and egg scenario: in PIM-SM multicast traffic is only forwarded when a router requests it with a PIM join message.  When 2 routers not directly connected to Mappin Agent want to receive traffic from 224.0.1.40, they’ll have to send a PIM join to the RP address for 224.0.1.40. However, they have no idea what the RP address is. How do we solve it? There are two options:

*PIM sparse-dense mode* is an extension to PIM sparse mode. It allows our multicast routers to use dense mode for multicast groups that don’t have an RP configured. When your router knows the RP, it will use sparse mode. If it doesn’t know the RP  will use dense mode. This allows traffic to 224.0.1.40 to be flooded throughout the network so that R2 and R6 can also learn the RP address.

The second option, *auto-rp listener* is a bit similar. If you enable this then the router will use dense mode only for the 224.0.1.39 and 224.0.1.40 addresses.

#### BSR

Uses PIMv2 and it is an IETF standard. A *BootStrapRouter* act smilarly to a Mapping Agent. Boostrap Router collects mapping information (c-RAP Advertisement) sent from one or more c-RP (candidate-RP). BSR than will send boostrap message to the neighbours PIM router with source IP the BSR interface. Using RPF, the PIM routers forward the boostrap messages via non-RPF interfaces and drop boostrap messages received via non-RPF interface (recieve ony on RPF interface). That make sure that all PIM routers will recieve a copy of boostrap and it will avoid loops.

Each PIM router will use the same hash algorithm to pick up the best RP

Multiple BSR are supported (c-BSR). They list their priority into BSM. The highest priority wind (in case of tie, the highest BSR IP win)

#### Anycast RP with MSDP *Mukticast Source Discovery Protocol*

Using Anycast RP is an implementation strategy that provides load sharing and redundancy in PIM-SM networks. Anycast RP allows two or more rendezvous points (RPs) to share the load for source registration and the ability to act as hot backup routers for each other. Using Anycast RP is an implementation strategy that provides load sharing and redundancy in PIM-SM networks. Anycast RP allows two or more rendezvous points (RPs) to share the load for source registration and the ability to act as hot backup routers for each other. The RP in each domain establishes an MSDP peering session using a TCP connection with the RPs in other domains or with border routers leading to the other domains. When the RP learns about a new multicast source within its own domain (through the normal PIM register mechanism), the RP encapsulates the first data packet in a Source-Active (SA) message and sends the SA to all MSDP peers. If the receiving MSDP peer is an RP, and the RP has a (*, G) entry for the group in the SA (there is an interested receiver), the RP creates (S, G) state for the source and joins to the shortest path tree for the source. The MSDP speaker periodically sends SAs that include all sources within the domain of the RP.

#### BiDir PIM - Designated Forwarder

Bidir-PIM use a concept of an elected Designated Forwarder (DF) that establishes a loop-free STP Shared Tree routed at the RP.

– On each point-to-point link and every network segment one DF is elected for every RP of Bidirectional group, the DF will be responsible for forwarding multicast traffic received on that network.

– The election of DF is based on the best unicast routing metric of the path to the RP, and consider only one path (no Assert messages) the order is as follow:

#### BiDir SSM & IGMPv3

The following two components together support the implementationof SSM:
• Protocol Independent Multicast source-specific mode (PIM-SSM)
• Internet Group Management Protocol Version 3 (IGMPv3)

PIM-SSM filter Multicast traffic based on source and destinationcan IP. PIM-SSM can run only on the last hop router. Is not required that the itemrediate routers run PIM-SSM (PIM-SSM is backward compatible with PIM-SM)

IGMPv3 introduces the ability for hosts to signal group membership that allows filtering capabilities with respect to sources.
A host can signal either that it wants to receive traffic from all sources sending to a group except for some specific sources
(a mode called EXCLUDE) or that it wants to receive traffic only from some specific sources sending to the group (a mode called INCLUDE).

