### point-to-point vs. point-to-multipoint
point-to-point has UNICAST, MULTICAST and BROADCAST capability. Beacause in a point-to-point link only one node is expected, the router should be able to reach the destination (connected route)

point-to-multipoint has only UNICAST capability. More than one router is expected on the other end of the tunnel, so the local router must have mapping for the next-hop otherwise NLRI cannot be achieved. So, multicast capability must be provided via configuration and next-hop IP router must statically defined

NBMA: has not multicast and broadcast capability< requires DR, not a BDR

### OSPF Cost

Cost = Reference bandwidth / Interface bandwidth in bps.

Cisco uses 100Mbps bandwidth as reference

1. Cost is a positive integer value.

2. Any decimal value would be rounded back in nearest positive integer.

3. Any value below 1 would be considered as 1.

1   Mbps line	1Mbps	  100000000/1000000     = 100

10  Mbps line	10Mbps	100000000/10000000    = 10

100 Mbps line	100Mbps	100000000/100000000   = 1

1   Gbps line	1Gbps	  100000000/1000000000  = 0.1	 --> 1

10  Gbps line	10Gbps	100000000/10000000000 = 0.01 --> 1

### LSA Throttling

OSPF LSA throttling is a feature that delays the generation of LSAs during network instability. Before LSA throttling, LSA generation was rate-limited to 5 seconds because of the default LSA-wait timer interval. This meant that an LSA could not be propagated in milliseconds and thus OSPF sub-second convergence was impossible.

This feature is very similar to OSPF SPF throttling. The mechanism behind it is exactly the same. It uses three values:

lsa-start: the initial wait interval for LSA generation. It is 0 milliseconds which means that the first LSA will be generated immediately.

lsa-hold: the minimum hold time interval between two LSA generations. The default value is 5000 milliseconds and it doubles every time the same LSA has to be re-generated.

lsa-max: the maximum wait time interval between two LSA generations. The default value is 5000 milliseconds. It is also used to limit the maximum value of the lsa-hold value.

The first LSA that is generated uses the lsa-start timer, so it is generated immediately. If the same LSA has to be re-generated, it will use the lsa-hold value instead. When the LSA has to be generated the third time, the lsa-hold value will double. Each time the LSA is re-generated, the lsa-hold value will double until it reaches the lsa-max value.

An LSA is considered the same if the following three values are the same:

LSA ID number

LSA type

advertising router ID

### OSPF SPF Scheduling and Throttling

When an OSPF router receives an updated LSA, it doesn’t run SPF right away but schedules it. It does so in case there is a change in the topology. However, the local router is probably not the only one affected so it’s likely that you will receive more than one updated LSA. The router waits for a short while so that it only has to run SPF once for all updated LSAs.

If the topology change is caused by a flapping link then the router would run SPF over and over again, increasing its load. To ensure this doesn’t happen, the delay before SPF runs will keep increasing if you keep receiving updated LSAs.

This scheduling of SPF is controlled by SPF throttling. There are three values that it uses:

spf-start: the initial wait interval before SPF starts after receiving an updated LSA.

spf-hold: the wait interval between subsequent SPF runs. This value doubles for each time SPF runs.

spf-max-wait: the maximum time between two SPF runs, used to cap the spf-hold value. It also defines how long the network has to be stable before the wait interval is reset to the spf-start and spf-hold values.

###  OSPF fast hello packets

OSPF fast hello packets are achieved by using the ip ospf dead-interval command. The dead interval is set to 1 second, and the hello-multiplier value is set to the number of hello packets you want sent during that 1 second, thus providing subsecond or "fast" hello packets.

When fast hello packets are configured on the interface, the hello interval advertised in the hello packets that are sent out this interface is set to 0. The hello interval in the hello packets received over this interface is ignored.

### Incremental SPF

 OSPF uses Dijkstra's SPF algorithm to compute the shortest path tree (SPT). During the computation of the SPT, the shortest path to each node is discovered. The topology tree is used to populate the routing table with routes to IP networks. When changes to a Type-1 or Type-2 link-state advertisement (LSA) occur in an area, the entire SPT is recomputed. In many cases, the entire SPT need not be recomputed because most of the tree remains unchanged. Incremental SPF allows the system to recompute only the affected part of the tree. Recomputing only a portion of the tree rather than the entire tree results in faster OSPF convergence and saves CPU resources. Note that if the change to a Type-1 or Type-2 LSA occurs in the calculating router itself, then the full SPT is performed.

Incremental SPF is scheduled in the same way as the full SPF. Routers enabled with incremental SPF and routers not enabled with incremental SPF can function in the same internetwork.

### OSPF Prefix Suppression

The OSPF mechanism to exclude connected IP prefixes from LSAs allows network administrators to control what IP prefixes are installed into LSAs. This functionality is implemented for router and network LSAs in the following manner:

• For the router LSA, to exclude prefixes, the feature excludes link type 3 (stub link).

• For the network LSA, the OSPF Designated Router (DR) generates LSAs with a special /32 network mask (0xFFFFFFFF)

You can reduce OSPF convergence time by configuring the OSPF process on a router to prevent the advertisement of all IP prefixes by using the `prefix-suppression` command in router configuration mode or per interface basis with the `ip ospf prefix-suppression` command. (Interface has priority over global)

### Remote loop-free alternate (LFA) IP fast reroute (IPFRR)

Per-link: all prefixes that are reachable through a certain link all share the same next hop address. An IGP can calculate a backup next hop for all prefixes that use the same link. When the link fails, all prefixes will automatically be assigned to use the same backup next hop address. The advantage of per link LFA is that it requires fewer CPU cycles and memory than per-prefix LFA. The downside, however, is that once the primary link fails, you suddenly put a lot of burden on the backup link.

Per-prefix: the IGP calculates an LFA for each and every prefix. It requires more CPU cycles and memory but it does offer better load balancing. When a primary path fails, prefixes could use different backup paths, spreading the traffic throughout the network.

When OSPF has to select a backup path, it doesn’t just look for the “next best” lowest metric path but it uses a list of “tie breakers” to decide what path to use. This process is a bit similar to how BGP uses attributes. OSPF is able to use the following tie breakers:

SRLG (Shared Risk Link Groups): this is a group of interfaces that have a high likelihood of failing at the same time. For example, VLAN interfaces that use the same physical interface. When one logical interface goes down, it’s very likely that the other logical interfaces on the same physical interface go down too. In the EIGRP LFA FRR lesson, you can find a configuration example for SRLG.

Interface Protection: don’t select an LFA that uses the same outgoing interface as the primary path.

Broadcast Interface Protection: don’t select backup paths that use the same broadcast network as the primary path. With a broadcast network (most likely a switch), you can have different next hops but you still use the same link. When the switch fails, there is a risk that both the primary and backup path are both unreachable.

Node Protection: don’t select a backup path that uses the same next-hop router as your primary path. I will demonstrate this one in the configuration section where it’s explained in detail.

Downstream Path: this is very similar to the EIGRP feasible successor rule. A neighbour should have a smaller metric to the destination as the total metric of our primary path. This attribute is added since traffic sent over backup paths might loop for a short time until OSPF recalculates the primary path.

Line-Card Disjoint Interfaces: this is similar to SRLGs, don’t use backup paths that use the same line card as the primary path.

Metric: the best backup path might not be the one with the lowest metric next to the primary path, which is why we have all these tie breaker attributes. However, you can still use the lowest metric as one of the tie breakers.

Equal-Cost Multipath:

    Primary: prefer a backup path that is part of ECMP (equal cost multipath).

    Secondary: prefer a backup path is not part of ECMP. This can be useful if a single link in ECMP is unable to handle all traffic. Imagine you have 2x 100 Mbit interfaces carrying about 150 Mbit of traffic. When one of the links fails, a single link will be unable to transmit everything. In this case, it’s best not to use the remaining link of the ECMP as a backup path.


### GTSM TTL Security Mechanism for OSPF

To filter network attacks originating from invalid sources traveling over multiple hops, the Generalized TTL Security Mechanism (GTSM), RFC 3682, is used to prevent the attacks. GTSM filters link-local addresses and allows for only one-hop neighbor adjacencies through the configuration of TTL value 255. The TTL value in the IP header is set to 255 when OSPF packets are originated, and checked on the received OSPF packets against the default GTSM TTL value 255 or the user configured GTSM TTL value, blocking unauthorized OSPF packets originated from TTL hops away.

### Graceful shutdown (Cisco doesn't support overload bit?)

To force the router into stub status (prior to reboot/shutdown), use the max-metric router-lsa router configuration command. This command will change the OSPF metric for all non-stub interfaces in the router LSA to 65535.

Note: The infinite metric in the router LSA does not force the other routers to ignore the path, just nudge them into using alternate paths. The other routers in the network will thus select alternate OSPF paths (if they exist), but not the potential non-OSPF paths. Those will be selected only after the actual router reboot/shutdown.

#### Graceful restart

Graceful restart works by informing OSPF neighbors that it is going to restart. The router that is going to restart is called the restarting router, the restarting process is called the graceful restart mode. It starts by sending a link-local type 9 LSA called the grace LSA with a specified time called the grace period:

##### ospf graceful restart grace lsa

When the neighbors of R2 receive the grace LSA, they will respond with an acknowledgment:

##### ospf graceful restart ls ack

R1 and R3, the neighbors of R2, are going to help with the graceful restart. These two routers will enter helper mode and are called the helper neighbors. During the grace period, R1 and R3 will act as if R2 is still out there even though they don’t receive any hello packets but will only do so if the topology doesn’t change during the graceful restart.

During the time that the router is restarting, there are no changes to the routing table and forwarding tables, so traffic will be forwarded as usual:

##### ospf graceful restart data forwarding during restart

Once OSPF has restarted, it will re-establish neighbor adjacencies with the helper routers:

##### ospf graceful restart re-establish adjacencies

When the graceful restart has completed, the restarting router flushes the grace LSA. The restarting router continues with its regular OSPF tasks. It will re-originate its LSAs to its neighbors:

##### ospf graceful flush grace lsa

The restarting router continues with its regular OSPF tasks. It will re-originate its LSAs to its neighbors:

##### ospf graceful restart originate lsa

And it will rerun SPF to refresh the routing table:

##### ospf graceful rerun spf

R2 has now successfully restarted while there was no network downtime.

OSPF graceful restart is covered in RFC 3623. Before the RFC, Cisco already implemented its own version of graceful restart called NSF (Non-Stop Forwarding). Cisco IOS supports both NSF and the RFC version of graceful restart.

Most routers support helper mode but only routers with specific hardware support graceful restart. This is because routers require autonomous hardware for forwarding that is separated from the CPU. Cisco calls devices that support helper mode NSF-aware and devices that support graceful restart are called NSF-capable.
