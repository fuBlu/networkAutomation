#### Default Route

*network 0.0.0.0*: A route to 0.0.0.0/0 must existed in RIB. Note - This command will redistribute 0.0.0.0/0 to all neighbors 

*redistribute*: Will inject def route into BGP neighbors only if def route exist in RIB table and if it has been learned from another routing protocol 

*default-information originate*: causes the default route to be artificially generated and injected into the BGP RIB, regardlessly
of whether it is present in the routing table. The newly injected default will be advertised to all BGP peers (because it now 
resides in the BGP RIB)

*neighbor X.X.X.X default-originate*: same as default-information originate with the onlydifference that def route is aderitsed to a specific neighbor. Def route doesn't have to exist into RIB and is artificially created into BGP RIB. Using *route-map* BGP can be forced to check RIB for an existing entry for 0.0.0.0/0. In case that entry does not exist, def route is not injected into BGP neighbor 

#### Regex

https://regex101.com

"^" start of a string

"$" end of a string

"|" logic OR applied to the preceding and succeding character. If preeceded by a value in parentheses, the logical applies 
  to the preceding string listed inside the parentheses

"_" any delimeter (blank space, comma, start of line or end of line

"." any single charachter

"?" zero or one instances of the preeceding charcater (True or False logic). Escape is ESC-Q

"*" zero or more instances of the preeceding charcater 

"+" one or more instances of the preeceding charcater 

"(string)" combine enclossed string characters as a single entty when used with ?, * or ?

"[string]" any of the single character in the string can be used to match (range logic) 

i.e. usgin route-server.opentransit.net

##### show ip bgp regexp _123+ (one or more instance of 3)

ia2.16.2.0/23      193.251.245.10         100     85      0 174 12322 12322 12322 20940 ?
 
ia1.18.127.0/24    193.251.245.92         100     85      0 174 4766 1237 23596 i

bia1.18.134.0/24    193.251.245.53         100     85      0 1239 3786 23600 i
 
##### show ip bgp regexp _.*(09)+ (any charachter before one ore more instance of 09)

 ia1.255.125.0/24   193.251.245.10         100     85      0 1299 9318 38091 9770 i
 
 ia2.16.2.0/23      193.251.245.10         100     85      0 174 12322 12322 12322 20940 ?
 
 i 12.233.151.0/24  193.251.245.3          100     75      0 701 46509 46509 i
 
 i                  193.251.245.1          100     85      0 2914 16509 i

##### show ip bgp regexp _[1-3]$ (any route sourced from ASN 1 or 2 or 3)

 ia200.52.157.0     193.251.245.57         100     85      0 1299 27932 1 i
 
 ia192.54.222.0     193.251.245.9          100     85      0 3356 3 i
 
 ia155.133.112.0/21 193.251.245.53         100     85      0 3356 197592 197592 2 i
 
#####  show ip bgp regexp (_[1-3]$)|(^174_)  (any route sourced from ASN 1, 2, 3 or any route received from ASN 174)

 ia24.106.96.0/23   193.251.245.10         100     85      0 174 7843 33363 i
 
 ia18.3.69.0/24     193.251.245.252          0    100      0 7922 7015 3 i


#### Router Capability

*Graceful Restart (or Non-Stop Forwarding)* - Support of End-of-RIB mark (used to inform neighbor that intial routing update is completed (Cisco use keepalive also). After a BGP router restart, the Restart State must be set to 1 to reestablish the peering session with the receiving BGP router. After the BGP session has reformed, the restarting BGP router waits untill it has received the End-of-RIB marker from the receiving BGP router. The restarting BGP router tha run BGP decision process, refreshes the forwarding table and update the receiving BGP router with the Adj-RIB-Out terminated by the end-of-RIB marker,

*Route Refresh* - Require an eBGP to resend and UPDATE for Adj-RIB-Out so the router can re-apply inbound policy. Route Refresh is mutually exclusive with Soft Reconfiguration

*Outbound Route Filtering (ORF)* - Can apply outbund filtering to an eBGP neighbour. Can be configured in (to recevie filtering only), out (to send filtering only) or both. ORF is not applicable to peer-group for inbound

#### Prefix Update Optimization

BGP Soft Reconfiguration *outbound* doesn't require any special configuration: the router sends a new UPDATE message with the update NLRI based on  new outbound policy. For *inbound* instead, it requires to be configured for specific peer. Also, double of the memory is required to mantain 2 tables. BGP peer still recieve the route and make it available on BGP table, however it won't install in RIB
