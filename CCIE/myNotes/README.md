#### CCIE R&S Blueprint resources available at:

https://mysecurecloud.ddns.net/owncloud/index.php/s/ZQwRe7qyVP71f0s

1.0 Network Principles
	
10%
	
1.1 Network theory

    1.1.a Describe basic software architecture differences between IOS and IOS XE
        1.1.a [i] Control plane and Forwarding plane
        1.1.a [ii] Impact to troubleshooting and performances
        1.1.a [iii] Excluding specific platform's architecture
    1.1.b Identify Cisco express forwarding concepts
        1.1.b [i] RIB, FIB, LFIB, Adjacency table
        1.1.b [ii] Load balancing Hash
        1.1.b [iii] Polarization concept and avoidance
    1.1.c Explain general network challenges
        1.1.c [i] Unicast flooding
        1.1.c [ii] Out of order packets
        1.1.c [iii] Asymmetric routing
        1.1.c [iv] Impact of micro burst
    1.1.d Explain IP operations
        1.1.d [i] ICMP unreachable, redirect
        1.1.d [ii] IPv4 options, IPv6 extension headers
        1.1.d [iii] IPv4 and IPv6 fragmentation
        1.1.d [iv] TTL
        1.1.d [v] IP MTU
    1.1.e Explain TCP operations
        1.1.e [i] IPv4 and IPv6 PMTU
        1.1.e [ii] MSS
        1.1.e [iii] Latency
        1.1.e [iv] Windowing
        1.1.e [v] Bandwidth delay product
        1.1.e [vi] Global synchronization
        1.1.e [vii] Options
    1.1.f Explain UDP operations
        1.1.f [i] Starvation
        1.1.f [ii] Latency
        1.1.f [iii] RTP/RTCP concepts

1.2 Network implementation and operation

    1.2.a Evaluate proposed changes to a network
        1.2.a [i] Changes to routing protocol parameters
        1.2.a [ii] Migrate parts of a network to IPv6
        1.2.a [iii] Routing protocol migration
        1.2.a [iv] Adding multicast support
        1.2.a [v] Migrate spanning tree protocol
        1.2.a [vi] Evaluate impact of new traffic on existing QoS design

1.3 Network troubleshooting

    1.3.a Use IOS troubleshooting tools
        1.3.a [i] debug, conditional debug
        1.3.a [ii] ping, traceroute with extended options
        1.3.a [iii] Embedded packet capture
        1.3.a [iv] Performance monitor
    1.3.b Apply troubleshooting methodologies
        1.3.b [i] Diagnose the root cause of networking issue [analyze symptoms, identify and describe root cause]
        1.3.b [ii] Design and implement valid solutions according to constraints
        1.3.b [iii] Verify and monitor resolution
    1.3.c Interpret packet capture
        1.3.c [i] Using Wireshark trace analyzer
        1.3.c [ii] Using IOS embedded packet capture

2.0 Layer 2 Technologies
	
13%

2.1 LAN switching technologies

    2.1.a Implement and troubleshoot switch administration
        2.1.a [i] Managing MAC address table
        2.1.a [ii] errdisable recovery
        2.1.a [iii] L2 MTU
    2.1.b Implement and troubleshoot layer 2 protocols
        2.1.b [i] CDP, LLDP
        2.1.b [ii] UDLD
    2.1.c Implement and troubleshoot VLAN
        2.1.c [i] Access ports
        2.1.c [ii] VLAN database
        2.1.c [iii] Normal, extended VLAN, voice VLAN
    2.1.d Implement and troubleshoot trunking
        2.1.d [i] VTPv1, VTPv2, VTPv3, VTP pruning
        2.1.d [ii] dot1Q
        2.1.d [iii] Native VLAN
        2.1.d [iv] Manual pruning
    2.1.e Implement and troubleshoot EtherChannel
        2.1.e [i] LACP, PAgP, manual
        2.1.e [ii] Layer 2, layer 3
        2.1.e [iii] Load-balancing
        2.1.e [iv] Etherchannel misconfiguration guard
    2.1.f Implement and troubleshoot spanning-tree
        2.1.f [i] PVST+/RPVST+/MST
        2.1.f [ii] Switch priority, port priority, path cost, STP timers
        2.1.f [iii] port fast, BPDUguard, BPDUfilter
        2.1.f [iv] loopguard, rootguard
    2.1.g Implement and troubleshoot other LAN switching technologies
        2.1.g [i] SPAN, RSPAN, ERSPAN
    2.1.h Describe chassis virtualization and aggregation technologies
        2.1.h [i] Multichassis
        2.1.h [ii] VSS concepts
        2.1.h [iii] Alternative to STP
        2.1.h [iv] Stackwise
        2.1.h [v] Excluding specific platform implementation
    2.1.i Describe spanning-tree concepts
        2.1.i [i] Compatibility between MST and RSTP
        2.1.i [ii] STP dispute, STP bridge assurance

2.2 Layer 2 multicast

    2.2.a Implement and troubleshoot IGMP
        2.2.a [i] IGMPv1, IGMPv2, IGMPv3
        2.2.a [ii] IGMP snooping
        2.2.a [iii] IGMP querier
        2.2.a [iv] IGMP filter
        2.2.a [v] IGMP proxy
    2.2.b Explain MLD
    2.2.c Explain PIM snooping

2.3 Layer 2 WAN circuit technologies

    2.3.a Implement and troubleshoot HDLC
    2.3.b Implement and troubleshoot PPP
        2.3.b [i] Authentication [PAP, CHAP]
        2.3.b [ii] PPPoE
        2.3.b [iii] MLPPP
    2.3.c Describe WAN rate-based ethernet circuits
        2.3.c [i] Metro and WAN Ethernet topologies
        2.3.c [ii] Use of rate-limited WAN ethernet services

3.0 Layer 3 Technologies
	
Hide Details

3.1 Addressing technologies

    3.1.a Identify, implement and troubleshoot IPv4 addressing and subnetting
        3.1.a [i] Address types, VLSM
        3.1.a [ii] ARP
    3.1.b Identify, implement and troubleshoot IPv6 addressing and subnetting
        3.1.b [i] Unicast, multicast
        3.1.b [ii] EUI-64
        3.1.b [iii] ND, RS/RA
        3.1.b [iv] Autoconfig/SLAAC, temporary addresses [RFC4941]
        3.1.b [v] Global prefix configuration feature
        3.1.b [vi] DHCP protocol operations
        3.1.b [vii] SLAAC/DHCPv6 interaction
        3.1.b [viii] Stateful, stateless DHCPv6
        3.1.b [ix] DHCPv6 prefix delegation

3.2 Layer 3 multicast

    3.2.a Troubleshoot reverse path forwarding
        3.2.a [i] RPF failure
        3.2.a [ii] RPF failure with tunnel interface
    3.2.b Implement and troubleshoot IPv4 protocol independent multicast
        3.2.b [i] PIM dense mode, sparse mode, sparse-dense mode
        3.2.b [ii] Static RP, auto-RP, BSR
        3.2.b [iii] BiDirectional PIM
        3.2.b [iv] Source-specific multicast
        3.2.b [v] Group to RP mapping
        3.2.b [vi] Multicast boundary
    3.2.c Implement and troubleshoot multicast source discovery protocol
        3.2.c [i] Intra-domain MSDP [anycast RP]
        3.2.c [ii] SA filter
    3.2.d Describe IPv6 multicast
        3.2.d [i] IPv6 multicast addresses
        3.2.d [ii] PIMv6

3.3 Fundamental routing concepts

    3.3.a Implement and troubleshoot static routing
    3.3.b Implement and troubleshoot default routing
    3.3.c Compare routing protocol types
        3.3.c [i] Distance vector
        3.3.c [ii] Link state
        3.3.c [iii] Path vector
    3.3.d Implement, optimize and troubleshoot administrative distance
    3.3.e Implement and troubleshoot passive interface
    3.3.f Implement and troubleshoot VRF lite
    3.3.g Implement, optimize and troubleshoot filtering with any routing protocol
    3.3.h Implement, optimize and troubleshoot redistribution between any routing protocol
    3.3.i Implement, optimize and troubleshoot manual and auto summarization with any routing protocol
    3.3.j Implement, optimize and troubleshoot policy-based routing
    3.3.k Identify and troubleshoot sub-optimal routing
    3.3.l Implement and troubleshoot bidirectional forwarding detection
    3.3.m Implement and troubleshoot loop prevention mechanisms
        3.3.m [i] Route tagging, filtering
        3.3.m [ii] Split horizon
        3.3.m [iii] Route poisoning
    3.3.n Implement and troubleshoot routing protocol authentication
        3.3.n [i] MD5
        3.3.n [ii] Key-chain
        3.3.n [iii] EIGRP HMAC SHA2-256bit
        3.3.n [iv] OSPFv2 SHA1-196bit
        3.3.n [v] OSPFv3 IPsec authentication

3.4 RIP [v2 and v6]

    3.4.a Implement and troubleshoot RIPv2
    3.4.b Describe RIPv6 [RIPng]

3.5 EIGRP [for IPv4 and IPv6]

    3.5.a Describe packet types
        3.5.a [i] Packet types [hello, query, update, and such]
        3.5.a [ii] Route types [internal, external]
    3.5.b Implement and troubleshoot neighbor relationship
        3.5.b [i] Multicast, unicast EIGRP peering
        3.5.b [ii] OTP point-to-point peering
        3.5.b [iii] OTP route-reflector peering
        3.5.b [iv] OTP multiple service providers scenario
    3.5.c Implement and troubleshoot loop free path selection
        3.5.c [i] RD, FD, FC, successor, feasible successor
        3.5.c [ii] Classic metric
        3.5.c [iii] Wide metric
    3.5.d Implement and troubleshoot operations
        3.5.d [i] General operations
        3.5.d [ii] Topology table, update, query, active, passive
        3.5.d [iii] Stuck in active
        3.5.d [iv] Graceful shutdown
    3.5.e Implement and troubleshoot EIGRP stub
        3.5.e [i] Stub
        3.5.e [ii] Leak-map
    3.5.f Implement and troubleshoot load-balancing
        3.5.f [i] equal-cost
        3.5.f [ii] unequal-cost
        3.5.f [iii] add-path
    3.5.g Implement EIGRP [multi-address] named mode
        3.5.g [i] Types of families
        3.5.g [ii] IPv4 address-family
        3.5.g [iii] IPv6 address-family
    3.5.h Implement, troubleshoot and optimize EIGRP convergence and scalability
        3.5.h [i] Describe fast convergence requirements
        3.5.h [ii] Control query boundaries
        3.5.h [iii] IP FRR/fast reroute [single hop]
        3.5.8 [iv] Summary leak-map
        3.5.h [v] Summary metric

3.6 OSPF [v2 and v3]

    3.6.a Describe packet types
        3.6.a [i] LSA yypes [1, 2, 3, 4, 5, 7, 9]
        3.6.a [ii] Route types [N1, N2, E1, E2]
    3.6.b Implement and troubleshoot neighbor relationship
    3.6.c Implement and troubleshoot OSPFv3 address-family support
        3.6.c [i] IPv4 address-family
        3.6.c [ii] IPv6 address-family
    3.6.d Implement and troubleshoot network types, area types and router types
        3.6.d [i] Point-to-point, multipoint, broadcast, non-broadcast
        3.6.d [ii] LSA types, area type: backbone, normal, transit, stub, NSSA, totally stub
        3.6.d [iii] Internal router, ABR, ASBR
        3.6.d [iv] Virtual link
    3.6.e Implement and troubleshoot path preference
    3.6.f Implement and troubleshoot operations
        3.6.f [i] General operations
        3.6.f [ii] Graceful shutdown
        3.6.f [iii] GTSM [Generic TTL Security Mechanism]
    3.6.g Implement, troubleshoot and optimize OSPF convergence and scalability
        3.6.g [i] Metrics
        3.6.g [ii] LSA throttling, SPF tuning, fast hello
        3.6.g [iii] LSA propagation control [area types, ISPF]
        3.6.g [iv] IP FRR/fast reroute [single hop]
        3.6.g [v] LFA/loop-free alternative [multi hop]
        3.6.g [vi] OSPFv3 prefix suppression

3.7 BGP

    3.7.a Describe, implement and troubleshoot peer relationships
        3.7.a [i] Peer-group, template
        3.7.a [ii] Active, passive
        3.7.a [iii] States, timers
        3.7.a [iv] Dynamic neighbors
    3.7.b Implement and troubleshoot IBGP and EBGP
        3.7.b [i] EBGP, IBGP
        3.7.b [ii] 4 bytes AS number
        3.7.b [iii] Private AS
    3.7.c Explain attributes and best-path selection
    3.7.d Implement, optimize and troubleshoot routing policies
        3.7.d [i] Attribute manipulation
        3.7.d [ii] Conditional advertisement
        3.7.d [iii] Outbound route filtering
        3.7.d [iv] Communities, extended communities
        3.7.d [v] Multi-homing
    3.7.e Implement and troubleshoot scalability
        3.7.e [i] Route-reflector, cluster
        3.7.e [ii] Confederations
        3.7.e [iii] Aggregation, AS set
    3.7.f Implement and troubleshoot multiproctocol BGP
        3.7.f [i] IPv4, IPv6, VPN address-family
    3.7.g Implement and troubleshoot AS path manipulations
        3.7.g [i] Local AS, allow AS in, remove private AS
        3.7.g [ii] Prepend
        3.7.g [iii] Regexp
    3.7.h Implement and troubleshoot other features
        3.7.h [i] Multipath
        3.7.h [ii] BGP synchronization
        3.7.h [iii] Soft reconfiguration, route refresh
    3.7.i Describe BGP fast convergence features
        3.7.i [i] Prefix independent convergence
        3.7.i [ii] Add-path
        3.7.i [iii] Next-hop address tracking

3.8 ISIS [for IPv4 and IPv6]

    3.8.a Describe basic ISIS network
        3.8.a [i] Single area, single topology
    3.8.b Describe neighbor relationship
    3.8.c Describe network types, levels and router types
        3.8.c [i] NSAP addressing
        3.8.c [ii] Point-to-point, broadcast
    3.8.d Describe operations
    3.8.e Describe optimization features
        3.8.e [i] Metrics, wide metric

4.0 VPN Technologies
	
13%

4.1 Tunneling

    4.1.a Implement and troubleshoot MPLS operations
        4.1.a [i] Label stack, LSR, LSP
        4.1.a [ii] LDP
        4.1.a [iii] MPLS ping, MPLS traceroute
    4.1.b Implement and troubleshoot basic MPLS L3VPN
        4.1.b [i] L3VPN, CE, PE, P
        4.1.b [ii] Extranet [route leaking]
    4.1.c Implement and troubleshoot encapsulation
        4.1.c [i] GRE
        4.1.c [ii] Dynamic GRE
        4.1.c [iii] LISP encapsulation principles supporting EIGRP OTP
    4.1.d Implement and troubleshoot DMVPN [single hub]
        4.1.d [i] NHRP
        4.1.d [ii] DMVPN with IPsec using preshared key
        4.1.d [iii] QoS profile
        4.1.d [iv] Pre-classify
    4.1.e Describe IPv6 tunneling techniques
        4.1.e [i] 6in4, 6to4
        4.1.e [ii] ISATAP
        4.1.e [iii] 6RD
        4.1.e [iv] 6PE/6VPE
    4.1.g Describe basic layer 2 VPN —wireline
        4.1.g [i] L2TPv3 general principals
        4.1.g [ii] ATOM general principals
    4.1.h Describe basic L2VPN — LAN services
        4.1.h [i] MPLS-VPLS general principals
        4.1.h [ii] OTV general principals

4.2 Encryption

    4.2.a Implement and troubleshoot IPsec with preshared key
        4.2.a [i] IPv4 site to IPv4 site
        4.2.a [ii] IPv6 in IPv4 tunnels
        4.2.a [iii] Virtual tunneling Interface [VTI]
    4.2.b Describe GET VPN

5.0 Infrastructure Security
	
5%
	
5.1 Device security

    5.1.a Implement and troubleshoot IOS AAA using local database
    5.1.b Implement and troubleshoot device access control
        5.1.b [i] Lines [VTY, AUX, console]
        5.1.b [ii] SNMP
        5.1.b [iii] Management plane protection
        5.1.b [iv] Password encryption
    5.1.c Implement and troubleshoot control plane policing
    5.1.d Describe device security using IOS AAA with TACACS+ and RADIUS
        5.1.d [i] AAA with TACACS+ and RADIUS
        5.1.d [ii] Local privilege authorization fallback

5.2 Network security

    5.2.a Implement and troubleshoot switch security features
        5.2.a [i] VACL, PACL
        5.2.a [ii] Stormcontrol
        5.2.a [iii] DHCP snooping
        5.2.a [iv] IP source-guard
        5.2.a [v] Dynamic ARP inspection
        5.2.a [vi] port-security
        5.2.a [vii] Private VLAN
    5.2.b Implement and troubleshoot router security features
        5.2.b [i] IPv4 access control lists [standard, extended, time-based]
        5.2.b [ii] IPv6 traffic filter
        5.2.b [iii] Unicast reverse path forwarding
    5.2.c Implement and troubleshoot IPv6 first hop security
        5.2.c [i] RA guard
        5.2.c [ii] DHCP guard
        5.2.c [iii] Binding table
        5.2.c [iv] Device tracking
        5.2.c [v] ND inspection/snooping
        5.2.c [vii] Source guard
        5.2.c [viii] PACL
    5.2.d Describe 802.1x
        5.2.d [i] 802.1x, EAP, RADIUS
        5.2.d [ii] MAC authentication bypass

6.0 Infrastructure Services
	
12%

6.1 System management

    6.1.a Implement and troubleshoot device management
        6.1.a [i] Console and VTY
        6.1.a [ii] telnet, HTTP, HTTPS, SSH, SCP
        6.1.a [iii] [T]FTP
    6.1.b Implement and troubleshoot SNMP
        6.1.b [i] v2c, v3
    6.1.c Implement and troubleshoot logging
        6.1.c [i] Local logging, syslog, debug, conditional debug
        6.1.c [ii] Timestamp

6.2 Quality of service

    6.2.a Implement and troubleshoot end-to-end QoS
        6.2.a [i] CoS and DSCP mapping
    6.2.b Implement, optimize and troubleshoot QoS using MQC
        6.2.b [i] Classification
        6.2.b [ii] Network based application recognition [NBAR]
        6.2.b [iii] Marking using IP precedence, DSCP, CoS, ECN
        6.2.b [iv] Policing, shaping
        6.2.b [v] Congestion management [queuing]
        6.2.b [vi] HQoS, sub-rate ethernet link
        6.2.b [vii] Congestion avoidance [WRED]
    6.2.c Describe layer 2 QoS
        6.2.c [i] Queuing, scheduling
        6.2.c [ii] Classification, marking

6.3 Network services

    6.3.a Implement and troubleshoot first-hop redundancy protocols
        6.3.a [i] HSRP, GLBP, VRRP
        6.3.a [ii] Redundancy using IPv6 RS/RA
    6.3.b Implement and troubleshoot network time protocol
        6.3.b [i] NTP master, client, version 3, version 4
        6.3.b [ii] NTP Authentication
    6.3.c Implement and troubleshoot IPv4 and IPv6 DHCP
        6.3.c [i] DHCP client, IOS DHCP server, DHCP relay
        6.3.c [ii] DHCP options
        6.3.c [iii] DHCP protocol operations
        6.3.c [iv] SLAAC/DHCPv6 interaction
        6.3.c [v] Stateful, stateless DHCPv6
        6.3.c [vi] DHCPv6 prefix delegation
    6.3.d Implement and troubleshoot IPv4 network address translation
        6.3.d [i] Static NAT, dynamic NAT, policy-based NAT, PAT
        6.3.d [ii] NAT ALG
    6.3.e Describe IPv6 network address translation
        6.3.e [i] NAT64
        6.3.e [ii] NPTv6

6.4 Network optimization

    6.4.a Implement and troubleshoot IP SLA
        6.4.a [i] ICMP, UDP, Jitter, VoIP
    6.4.b Implement and troubleshoot tracking object
        6.4.b [i] Tracking object, tracking list
        6.4.b [ii] Tracking different entities [e.g. interfaces, routes, IPSLA, and such]
    6.4.c Implement and troubleshoot netflow
        6.4.c [i] Netflow v5, v9
        6.4.c [ii] Local retrieval
        6.4.c [iii] Export [configuration only]
    6.4.d Implement and troubleshoot embedded event manager
        6.4.d [i] EEM policy using applet
    6.4.e Identify performance routing [PfR]
        6.4.e [i] Basic load balancing
        6.4.e [ii] Voice optimization

7.0 Evolving Technologies
	
10%
	
7.1 Cloud

    7.1.a Compare and contrast Cloud deployment models
        7.1.a [i] Infrastructure, platform, and software services [XaaS]
        7.1.a [ii] Performance and reliability
        7.1.a [iii] Security and privacy
        7.1.a [iv] Scalability and interoperability
    7.1.b Describe Cloud implementations and operations
        7.1.b [i] Automation and orchestration
        7.1.b [ii] Workload mobility
        7.1.b [iii] Troubleshooting and management
        7.1.b [iv] OpenStack components

7.2 Network programmability [SDN]

    7.2.a Describe functional elements of network programmability [SDN] and how they interact
        7.2.a [i] Controllers
        7.2.a [ii] APIs
        7.2.a [iii] Scripting
        7.2.a [iv] Agents
        7.2.a [v] Northbound vs. Southbound protocols
    7.2.b Describe aspects of virtualization and automation in network environments
        7.2.b [i] DevOps methodologies, tools and workflows
        7.2.b [ii] Network/application function virtualization [NFV, AFV]
        7.2.b [iii] Service function chaining
        7.2.b [iv] Performance, availability, and scaling considerations

7.3 Internet of Things

    7.3.a Describe architectural framework and deployment considerations for Internet of Things [IoT]
        7.3.a [i] Performance, reliability and scalability
        7.3.a [ii] Mobility
        7.3.a [iii] Security and privacy
        7.3.a [iv] Standards and compliance
        7.3.a [v] Migration
        7.3.a [vi] Environmental impacts on the network
