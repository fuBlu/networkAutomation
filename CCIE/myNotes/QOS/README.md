### IntServ vs DiffServ

*IntServ* uses a flow-based concept coupled with a signaling protocol along the packet path (RSVP) The signaling protocol guarantees thayt adequate resources are available for the flow before admitting the flow onto network

*DiffServ* uses packet markings to classify and treat the packet indipendently

### Classification and Marking Tools

### Classification

1 - VIa ACL (From layer 2 to layer 4)
2 - Layer 7 via NBAR (DPI) - CEF required

##### Class Map
Uses *match-any* (logical OR) or *match-all* (logical OR). Default behavior is *match-all*
The class-map order into a policy-map it does matter. (Evaluated in top to bottom order, first match)

### Marking

1 - IPP L3 (IP Precedence) - Uses 3 leftmost bits of 8-bits TOS field on IP Packet

CS0 - 000000 - 000

CS1 - 001000 - 001

CS2 - 010000 - 010

CS3 - 011000 - 011

CS4 - 100000 - 100

CS5 - 101000 - 101

CS6 - 110000 - 110

CS7 - 110000 - 111

2 - DSCP L3 - Uses 6 leftmost bits of 8-bits TOS field on IP Packet - Backward compatible with IPP. DSCP Keyword Name is also called *Per-Hop Behaviors (PHB)*

PHB - Best Effort; Assured Forwarding AFxy; Expedited Forwardinf EF; Class-Selecotr CSx (backward compatible with IPP)

AFxy - x Queue, y Drop Probability
y can have value of 1, 2 or 3. 1 *Lowest* drop preference, 3 *Highest* drop preference
To convert AFxy to decimal equivalent - 8x + 2y i.e. (8x4)+(2x1) = AF41

3 - COS L2

3.1 - 802.1p - Layer 2 mark in 802.1Q frame header - 3 bits only available - 1 to 1 matche with IPP

3.2 - MPLS EXP - 3 bits

3.3 - ATM CLP and Frame Relay DE - 1 bit

NOTE: L2 mark si lost whne media type change (i.e from ethernet to MPLS) - Traffic must be re-calssified

### Policing and Shaping

##### Token Bucket Filter

Traffic send out to network interface must sent at link rate speed. (i.e. 128.000 kbps cannot send at 92.000 kbps). To overcome this problem, line rate can be imposed used Time Division Multiplexing.

Tc - Time interval, measured in millisencond, over which committed burst (Bc) can be sent. Tc cannot set on connfig but can be obteined from Tc = Bc/CIR. i.e in VOIP or Video application Tc=10ms is suggested, so BC = CIR/100 

Bc - Commited burst size, measured in bits, this is the amount of traffic that can be sent during 1/2 Tc (1/2 Tc bits are sent, 1/2 Tc data are stopped

CIR - Committed information rate, in bits per second, is the rate dfined

Be - Excess burst size, in bits. This is the number of bits beyond Bc that can be sent after a period on inactivity

Tc = Bc/CIR

##### Class-Based Policing 

1. Can be applied In or Out direction under interface
2. Bucket replanished at Policing Rate (i.e policing @96Kbps over 1 second, adds 12.000 tokens. Where 1 Token = 1 byte)
3. Does not allow excess busrts 
4. Can permit, drop or re-mark

*Single-Rate, Two-Color (One Bucket)*

*Conform* if Xp <= Xb

*Exceed*  if Xp => Xb

*Single-Rate, Three-Colors (Two Buckets)*

If Bc has any token left, some of them will spill on a second bucket for a maximum of Be size

*Conform* if Xp <= Xbc

*Exceed*  if Xp > Xbc and  Xp <= Xbe

*Violate* if Xp => Xbe

*Two-Rate, Three-Colors (Two Buckets)*

Peak Information Rate (PIR) is the higher rate and CIR is the lower rate. Packets are checked against PIR first (that must be bigger that CIR). If traffic is not violate, is checked against CIR after .

*Conform* if Xp <= Xbc

*Exceed*  if Xp > Xbc and  Xp <= Xbe

*Violate* if Xp > Xbc and Xp > Xbe

CIR and PIR can be refileld at 2 different rates

Difference between single-rate and Two-rate - On single-rate Be is based on token spilled from Bc bucket (a period of low activity has to occur to refil the Be bucket). Two-rate has its own re-fill rate (PIR and CIR). Bc is the bucket size for CIR and Be is buckt sixe for PIR

##### Class-Based Shaping 

1. Can be applied only on Out direction under interface
2. Bucket replanished at Tc (1/2 Tc data sent, 1/2 data stop)
3. Allows excess busrts and buffer execive traffic
4. Can buffer traffic over CIR and release when traffic is below CIR

Be tokens allow burst of traffic over the CIR. Be are based on Bc token left in the bucket on previous Tc

*shape-average vs. shape-peak*

shape-peak: replenish Bc and Be buckets at every TC

shape-average: Be bucket uses Bc's leftover tokens

PeakRate = CIR (1 + Bc / Be)

### Scheduling - Congestion-Management

*Queuing* is the logic of ordering packet in linked output buffers. The process is enaged only when congestion is experienced and deactivated when congestion clear.

*Scheduling* is the process of deciding which packet to transimt next. Scheduling occours whne interface experience congestion but also when is not (there is still a decision of which packet should be transmitted next)

Congestion Management tools handle the head of the queue.

On cisco IOS there are 2 types of queue: *software queue* that is buffer/memory that hold the packets when congestion is experience; *hardware queue* also called as *Tx ring* or *circular buffer* is the queue that send the packet to the idriver interface to be sent. The hardware queue can be manipulate just in lenght and always uses FIFO. Congestion happens when the Tx Ring is full.

##### Class-Based Weighted Fair Queuing (CBWFQ)

CBWFQ scheduler guarantees a minimum percentage of link' s bandwidth fo each class/queue. If bandwidth allocate for a specific queue is not in use, it can be fairly redistribuedt among betwee the other queues.
*calss-default* is the default class not specified in configuration. It uses WFQ (fair-queue) where bandiwdth is allocated in relation to the IPP or DSCP value. None of the queues are service with priority from CBWFQ - FIFO is used a scheduler

##### Low-Latency Queue (LLQ)

LLQ adds low-latency queue to CBWFQ and it also prevents the queue starvation: the bandwidth given to LLQ priority queue is both the guaranteed minimum and policed maximum. As a result, the packets that make it out the queue experience low latency but some might be discarded to prevent starving the other queues. i.e. 10 VOIP calls are supported on LLQ and a 11th call is made it. LLQ starts to drop packets (that belong to the eisting calls as well as the 11th call). This is because the DIffServ doesn' t include andy bandwidht reservation like CAC.
If multiple LLQ are configured, they are both served first using FIFO between them.

### WRED and ECN - Congestion-Avoidance

WRED is implemented to avoid (or at least reduce) TCP *Global Synchronization* that is, when a queue is full the router drops indiscriminately all packets in that queue. All TCP flows start to reduce the transimission rate (due to sliding window mechanism) and gradually to ramp-up the tramission rate again)

WRED drops pakcet based on IPP or DSCP (lower IPP or DSCP, better probability for the packet to be dropped - Note: AF21 has less probability to be dropped that AF22. AF22 has less probability to be dropped that AF23)
WRED select 3 values: min threshod, max threshold and mark probability demoniator (MPD). When the *average depth* is below the min threshold packets are forwarded. When average depth is between min and max threshold, packet are discarded based on MPD value (i.e. MPD=1/10 means discard from 0 to 10%; MPD=1/20 means discard from 0 to 5%)

Wred is usually enabled on *class-default* with Error Congestion Notification (ECN)

#### ECN

ECN is used in conjunction with ECN and uses the rightmost 2 bits of TOS field:

ECN-Capable Transport (ETC) bit: Inidcates wheter the devices support ECN

Congestion Experienced (CE): This bit in conjuction with the ECT indicates wether congestion was experienced

When the average depth is between min and max threshold, the router mark the paket (without drop it) with CE bit and notify the sender/receiver that congestion is happening. Thus, the devices start to decrease the linerate. This happen without or with few packet drops
