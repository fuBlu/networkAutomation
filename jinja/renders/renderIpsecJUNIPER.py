#!/usr/bin/env python3
from jinja2 import Environment, FileSystemLoader
from netaddr import IPAddress
import yaml, sys, itertools

def main():

    appDicTCP = { 80:'junos-http', 443:'junos-https', 22:'junos-ssh', 21:'junos-ftp', 53:'junos-dns-tcp', 'any':'junos-tcp-any'}
    appDicUDP = { 123:'junos-ntp', 25:'junos-snmp', 53:'junos-dns-udp', 514:'junos-syslog', 'any':'junos-udp-any' }
    dstAddress = []
    srcAddress = []

    ENV = Environment(loader=FileSystemLoader('.'))
    template = ENV.get_template(sys.argv[1])

    # print jinja templatem
    with open(sys.argv[2], 'r') as f:
        yamlVar = yaml.load(f)
    print(template.render(yamlVar=yamlVar))

    # create traffic selectors
    for traffic in yamlVar[6]['trafficSelector']:
        for t in traffic:
            for selectors in traffic.values():
                for srcDst,ip in selectors.items():
                    if srcDst == 'source':
                        srcAddress.append(ip)
                        print('set security ipsec vpn {} traffic-selector {} local-ip {}'.format(yamlVar[3]['crypto']['cryptoNumber'],t.upper(),ip))
                    elif srcDst == 'destination':
                        dstAddress.append(ip)
                        print('set security ipsec vpn {} traffic-selector {} remote-ip {}'.format(yamlVar[3]['crypto']['cryptoNumber'],t.upper(),ip))

    # create source nd destination address book
    for src in srcAddress:
        if '/32' in src:
            print('set security address-book {} address H-{} {}'.format(yamlVar[2]['virtualInterface']['secZone'],src.replace('/', '-'),src))
            print('set security address-book {} address-set SG-{} address {}'.format(yamlVar[2]['virtualInterface']['secZone'],yamlVar[0]['SNOW']['change'],src))
        else:
            print('set security address-book {} address N-{} {}'.format(yamlVar[2]['virtualInterface']['secZone'],src.replace('/', '-'),src))
            print('set security address-book {} address-set SG-{} address {}'.format(yamlVar[2]['virtualInterface']['secZone'],yamlVar[0]['SNOW']['change'],src))
    for dst in dstAddress:
        if '/32' in dst:
            print('set security address-book UNTRUST address H-{} {}'.format(dst.replace('/', '-'),dst))
            print('set security address-book UNTRUST address-set DG-{} address {}'.format(yamlVar[0]['SNOW']['change'],dst))
        else:
            print('set security address-book UNTRUST address N-{} {}'.format(dst.replace('/', '-'),src))
            print('set security address-book UNTRUST address-set DG-{} address {}'.format(yamlVar[0]['SNOW']['change'],dst))

    print('set security policies from-zone TRUST to-zone {} policy {} match source-address SG-{}'.format(yamlVar[2]['virtualInterface']['secZone'],yamlVar[0]['SNOW']['change'],yamlVar[0]['SNOW']['change']))
    print('set security policies from-zone TRUST to-zone {} policy {} match destination-address DG-{}'.format(yamlVar[2]['virtualInterface']['secZone'],yamlVar[0]['SNOW']['change'],yamlVar[0]['SNOW']['change']))

    # find existin application or create new
    for key in yamlVar:
        if 'protPortDestination' in key:
            if key['protPortDestination']['protocol'] == 'tcp':
                for port in key['protPortDestination']['ports']:
                    if port in appDicTCP.keys():
                        print('set security policies from-zone TRUST to-zone {} policy {} match application {}'.format(yamlVar[2]['virtualInterface']['secZone'],yamlVar[0]['SNOW']['change'],appDicTCP[port]))
                    elif port not in appDicTCP.keys():
                        print('set applications application TCP-{} protocol tcp destination-port {}'.format(port,port))
                        print('set security policies from-zone TRUST to-zone {} policy {} match application TCP-{}'.format(yamlVar[2]['virtualInterface']['secZone'],yamlVar[0]['SNOW']['change'],port))

            elif key['protPortDestination']['protocol'] == 'udp':
                for port in key['protPortDestination']['ports']:
                    if port in appDicUDP.keys():
                        print('set security policies from-zone TRUST to-zone {} policy {} match application {}'.format(yamlVar[2]['virtualInterface']['secZone'],yamlVar[0]['SNOW']['change'],appDicUDP[port]))
                    elif port not in appDicUDP.keys():
                        print('set applications application UDP-{} protocol udp destination-port {}'.format(port,port))
                        print('set security policies from-zone TRUST to-zone {} policy {} match application UDP-{}'.format(yamlVar[2]['virtualInterface']['secZone'],yamlVar[0]['SNOW']['change'],port))

if __name__ == "__main__":
    main()
    '''Ver 1.0 f180521'''
